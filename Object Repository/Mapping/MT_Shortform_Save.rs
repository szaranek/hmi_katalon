<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MT_Shortform_Save</name>
   <tag></tag>
   <elementGuidId>d55d17d2-6652-403d-954a-5bf544f85486</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>arn-icon arn-icon-save-green</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app&quot;]/app-root[1]/div[@class=&quot;app has-header&quot;]/div[@class=&quot;content&quot;]/arn-test-gui[@class=&quot;ng-star-inserted&quot;]/app-dock-board[@class=&quot;has-toolbar ng-star-inserted&quot;]/app-dock[@class=&quot;mat-elevation-z4 ng-star-inserted&quot;]/div[@class=&quot;dock-component ng-star-inserted&quot;]/arn-mt-standard-text-dock-wrap[@class=&quot;ng-star-inserted&quot;]/arn-manual-translation-standard-text[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;mt-holder&quot;]/div[@class=&quot;col-12 content-content&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-6&quot;]/div[@class=&quot;form-group translation-container&quot;]/div[@class=&quot;translation-controls&quot;]/arn-manual-translation-mapping-demo[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;mapping-demo&quot;]/div[@class=&quot;mapping-editor&quot;]/div[@class=&quot;save-buttons ng-star-inserted&quot;]/button[@class=&quot;mat-md save-button mat-icon-button&quot;]/span[@class=&quot;mat-button-wrapper&quot;]/i[@class=&quot;arn-icon arn-icon-save-green&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='st'])[1]/following::i[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Last modified'])[1]/preceding::i[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='John Doe'])[2]/preceding::i[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//button[2]/span/i</value>
   </webElementXpaths>
</WebElementEntity>
