<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mapping_icon_translation_column</name>
   <tag></tag>
   <elementGuidId>89eb7d9d-ebc8-4624-8c71-3e3b027067c3</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-link</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mt-treeGrid-container&quot;)/arn-manual-translation-tree-grid[1]/div[@id=&quot;DefaultView&quot;]/div[@class=&quot;GridMain TSMain TSNormal TSWK&quot;]/table[@id=&quot;DefaultView&quot;]/tbody[1]/tr[3]/td[1]/div[@class=&quot;TSBodyMid&quot;]/div[1]/div[@class=&quot;TSSectionScroll&quot;]/div[@class=&quot;TSPageOne&quot;]/table[@class=&quot;TSSection&quot;]/tbody[1]/tr[3]/td[1]/div[1]/table[@class=&quot;TSSection&quot;]/tbody[1]/tr[3]/td[1]/div[1]/table[@class=&quot;TSSection&quot;]/tbody[1]/tr[@class=&quot;TSDataRow  TSClassSelected&quot;]/td[@class=&quot;TSClassNoFocus TSClassSelected TSCellBase TSType TSHtml TSCell HideCol0translation&quot;]/div[1]/div[1]/i[@class=&quot;fa fa-link&quot;]</value>
   </webElementProperties>
</WebElementEntity>
