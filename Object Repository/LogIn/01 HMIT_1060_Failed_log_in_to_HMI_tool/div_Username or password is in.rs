<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Username or password is in</name>
   <tag></tag>
   <elementGuidId>fd24617a-0dc0-4bee-854f-d7f8899ca901</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger mb-2 text-center ng-star-inserted</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Username or password is incorrect</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app&quot;]/app-root[1]/div[@class=&quot;app&quot;]/div[@class=&quot;content&quot;]/ng-component[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;login-page&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;content row&quot;]/div[@class=&quot;col-6 right-side&quot;]/div[@class=&quot;row justify-content-md-center&quot;]/div[@class=&quot;col-8&quot;]/form[@class=&quot;ng-dirty ng-touched ng-valid&quot;]/div[@class=&quot;alert alert-danger mb-2 text-center ng-star-inserted&quot;]</value>
   </webElementProperties>
</WebElementEntity>
