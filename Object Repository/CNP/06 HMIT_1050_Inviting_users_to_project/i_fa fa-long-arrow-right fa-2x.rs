<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_fa fa-long-arrow-right fa-2x</name>
   <tag></tag>
   <elementGuidId>e3966647-7d35-4272-9cb5-cccb7b8817ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-long-arrow-right fa-2x</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app modal-open&quot;]/ngb-modal-window[@class=&quot;modal fade custom-modal show&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;material-version ng-star-inserted&quot;]/div[@class=&quot;container-wrap&quot;]/div[@class=&quot;body-container&quot;]/div[@class=&quot;min-h-400px position-relative mb-2&quot;]/div[@class=&quot;modal-body-content ng-star-inserted&quot;]/app-invite-users[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;body invite-users-body&quot;]/div[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-2 align-self-center arrows&quot;]/button[@class=&quot;btn&quot;]/i[@class=&quot;fa fa-long-arrow-right fa-2x&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>cnp-inviteUsers-button-selectUser</value>
   </webElementProperties>
</WebElementEntity>
