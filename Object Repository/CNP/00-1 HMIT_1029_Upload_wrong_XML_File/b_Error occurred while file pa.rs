<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>b_Error occurred while file pa</name>
   <tag></tag>
   <elementGuidId>47139a45-28fc-423d-be59-3c95097439cf</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>b</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Error occurred while file parsing</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app modal-open&quot;]/ngb-modal-window[@class=&quot;modal fade custom-modal show&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;material-version ng-star-inserted&quot;]/div[@class=&quot;container-wrap&quot;]/div[@class=&quot;body-container&quot;]/div[@class=&quot;min-h-400px position-relative mb-2&quot;]/div[@class=&quot;modal-body-content ng-star-inserted&quot;]/app-upload-xml[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;body&quot;]/div[@class=&quot;ng-star-inserted&quot;]/app-error-details[2]/div[@class=&quot;row mt-3 error-wrapper ng-star-inserted&quot;]/div[@class=&quot;col-11 error-content&quot;]/div[@class=&quot;pt-5px&quot;]/b[1]</value>
   </webElementProperties>
</WebElementEntity>
