<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Add New_Font_Bitmap</name>
   <tag></tag>
   <elementGuidId>2df22a57-70bc-4d73-bebb-c4b9b35e2616</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@tid = 'cnp-metaData-fonts-button-addNew-Bitmap']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>tr:nth-child(3) > td:nth-child(5) > button[type=&quot;button&quot;] > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-button-wrapper</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Add New
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app modal-open&quot;]/ngb-modal-window[@class=&quot;modal fade custom-modal show&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;material-version ng-star-inserted&quot;]/div[@class=&quot;container-wrap&quot;]/div[@class=&quot;body-container&quot;]/div[@class=&quot;min-h-400px position-relative mb-2&quot;]/div[@class=&quot;modal-body-content ng-star-inserted&quot;]/app-meta-data[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;body project-validators-body&quot;]/table[@class=&quot;table validators-list mt-4 ng-star-inserted&quot;]/tbody[@class=&quot;ng-star-inserted&quot;]/tr[@class=&quot;validator-row font-files ng-star-inserted&quot;]/td[5]/button[@class=&quot;mat-md m-0 w-100 without-transform mat-raised-button&quot;]/span[@class=&quot;mat-button-wrapper&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>cnp-metaData-fonts-button-addNew-Bitmap</value>
   </webElementProperties>
</WebElementEntity>
