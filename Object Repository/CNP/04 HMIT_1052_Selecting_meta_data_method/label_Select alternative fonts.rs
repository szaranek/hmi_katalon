<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Select alternative fonts</name>
   <tag></tag>
   <elementGuidId>cde361f6-66ca-450a-8f3b-7010949d7e1f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Select alternative fonts for languages in the project.</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app modal-open&quot;]/ngb-modal-window[@class=&quot;modal fade custom-modal show&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;material-version ng-star-inserted&quot;]/div[@class=&quot;container-wrap&quot;]/div[@class=&quot;body-container&quot;]/div[@class=&quot;min-h-400px position-relative mb-2&quot;]/div[@class=&quot;modal-body-content ng-star-inserted&quot;]/app-alternative-fonts[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;body project-alternative-fonts-body&quot;]/div[@class=&quot;form-group create-project-subtitle&quot;]/label[1]</value>
   </webElementProperties>
</WebElementEntity>
