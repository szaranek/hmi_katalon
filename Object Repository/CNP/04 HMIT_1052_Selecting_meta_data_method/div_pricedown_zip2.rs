<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_pricedown_zip2</name>
   <tag></tag>
   <elementGuidId>917e6b96-ed73-431f-9fd7-9cfb29852b37</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[count(. | //*[contains(.,'
        pricedown.zip    ')]) = count(//*[contains(.,'
        pricedown.zip    ')])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ngb-popover-window#ngb-popover-4 div:nth-child(6)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>lang-row ng-star-inserted</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        pricedown.zip    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ngb-popover-2&quot;)/div[@class=&quot;popover-body&quot;]/div[@class=&quot;lang-row ng-star-inserted&quot;]</value>
   </webElementProperties>
</WebElementEntity>
