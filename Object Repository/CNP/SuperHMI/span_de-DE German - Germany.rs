<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_de-DE German - Germany</name>
   <tag></tag>
   <elementGuidId>edafaae3-110e-42ba-a6da-69bd6d2ee607</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@tid = 'cnp-languageNaming-option-de-DE']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>cnp-languageNaming-option-de-DE</value>
   </webElementProperties>
</WebElementEntity>
