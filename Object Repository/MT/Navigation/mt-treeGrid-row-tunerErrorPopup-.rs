<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mt-treeGrid-row-tunerErrorPopup-</name>
   <tag></tag>
   <elementGuidId>375981d5-9b7b-4c9c-b7a6-a517c11f87e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[starts-with(@tid, 'mt-treeGrid-row-tunerErrorPopup-')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-treeGrid-row-tunerErrorPopup-</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <value>//div[@id='mt-treeGrid-row-tunerErrorPopup-g17']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//table[@id='DefaultView']/tbody/tr[3]/td/div/div/div/div/table/tbody/tr[3]/td/div/table/tbody/tr[7]/td/div/table/tbody/tr[2]/td[3]/div/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tuner_ErrorPopup'])[1]/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//tr[7]/td/div/table/tbody/tr[2]/td[3]/div/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
