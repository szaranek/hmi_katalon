<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>08_mt-targetExport-footer-button-close</name>
   <tag></tag>
   <elementGuidId>58aa1a5e-4f8d-4f57-a078-f37c1d1da9ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[(text() = 'close' or . = 'close')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-targetExport-footer-button-close</value>
   </webElementProperties>
</WebElementEntity>
