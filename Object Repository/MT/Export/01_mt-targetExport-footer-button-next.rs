<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>01_mt-targetExport-footer-button-next</name>
   <tag></tag>
   <elementGuidId>df8b1209-9624-4f7b-948f-aca5a6a2709d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[(text() = '
                                next
                            ' or . = '
                                next
                            ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-targetExport-footer-button-next</value>
   </webElementProperties>
</WebElementEntity>
