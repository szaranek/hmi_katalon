<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>04-0_mt-targetExport-addNewFilter-input-name</name>
   <tag></tag>
   <elementGuidId>474d27f6-32ba-46ae-9f7b-02c34550b45c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@placeholder = 'Filter name']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-targetExport-addNewFilter-input-name</value>
   </webElementProperties>
</WebElementEntity>
