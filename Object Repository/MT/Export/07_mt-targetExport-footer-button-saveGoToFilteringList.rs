<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>07_mt-targetExport-footer-button-saveGoToFilteringList</name>
   <tag></tag>
   <elementGuidId>eaa590d5-abec-48d4-9c73-d237e84eacf1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[(text() = 'save &amp; go to filter list
                        ' or . = 'save &amp; go to filter list
                        ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-targetExport-footer-button-saveGoToFilteringList</value>
   </webElementProperties>
</WebElementEntity>
