<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>05_mt-targetExport-addNewFilter-inputSearchQuery</name>
   <tag></tag>
   <elementGuidId>06c62f01-7907-4265-bff8-e6ced60b11af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//textarea[@placeholder = 'advanced search query']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-targetExport-addNewFilter-inputSearchQuery</value>
   </webElementProperties>
</WebElementEntity>
