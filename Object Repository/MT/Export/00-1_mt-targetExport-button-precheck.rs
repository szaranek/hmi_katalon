<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>00-1_mt-targetExport-button-precheck</name>
   <tag></tag>
   <elementGuidId>0ea5fa58-b07d-4a5d-b97f-98308801a9bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[(text() = '
    Precheck
' or . = '
    Precheck
')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-targetExport-button-precheck</value>
   </webElementProperties>
</WebElementEntity>
