<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>03_mt-targetExport-filtering-button-addNextFilter</name>
   <tag></tag>
   <elementGuidId>f3af613e-16ff-4f30-ac02-dbbe8f22d2cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[(text() = '
        add_circle_outline
        Add next filter
    ' or . = '
        add_circle_outline
        Add next filter
    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-targetExport-filtering-button-addNextFilter</value>
   </webElementProperties>
</WebElementEntity>
