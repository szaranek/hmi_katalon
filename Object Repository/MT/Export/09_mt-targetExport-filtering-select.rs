<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>09_mt-targetExport-filtering-select</name>
   <tag></tag>
   <elementGuidId>df31a2b4-805b-4c86-84c4-b0b7e69e17b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'mat-select-value' and (text() = ' ' or . = ' ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-targetExport-filtering-select</value>
   </webElementProperties>
</WebElementEntity>
