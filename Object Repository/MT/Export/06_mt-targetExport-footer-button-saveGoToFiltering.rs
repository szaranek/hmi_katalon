<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>06_mt-targetExport-footer-button-saveGoToFiltering</name>
   <tag></tag>
   <elementGuidId>fd08d335-9da9-42c2-867c-2cdc18277ac9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[(text() = 'save &amp; go to filtering
                        ' or . = 'save &amp; go to filtering
                        ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-targetExport-footer-button-saveGoToFiltering</value>
   </webElementProperties>
</WebElementEntity>
