<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>02_mt-targetExport-filtering-button-editFilters</name>
   <tag></tag>
   <elementGuidId>20fb6766-df51-4f1f-8b3b-af0e3523a667</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[(text() = '
        edit
        Edit filters
    ' or . = '
        edit
        Edit filters
    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-targetExport-filtering-button-editFilters</value>
   </webElementProperties>
</WebElementEntity>
