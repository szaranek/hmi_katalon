<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>05_mt-languages-editFilters-button-addFilter</name>
   <tag></tag>
   <elementGuidId>a162081a-ae05-4aa4-9acf-6d865f3f355f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>add-more mat-button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-languages-editFilters-button-addFilter</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        add_circle_outline
        Add next filter
    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app modal-open&quot;]/ngb-modal-window[@class=&quot;modal fade custom-modal show&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;container-wrap ng-star-inserted&quot;]/div[@class=&quot;body-container&quot;]/div[@class=&quot;min-height ng-star-inserted&quot;]/arn-filter-list[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;translation-request&quot;]/button[@class=&quot;add-more mat-button&quot;]</value>
   </webElementProperties>
</WebElementEntity>
