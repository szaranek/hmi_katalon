<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mat-option_Check all</name>
   <tag></tag>
   <elementGuidId>d9d7e082-c067-42bf-abaa-50ac04c5dfb3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@tid = 'mt-languages-input-translationLanguagesbutton-checkAll']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-languages-input-translationLanguagesbutton-checkAll</value>
   </webElementProperties>
</WebElementEntity>
