<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Assign</name>
   <tag></tag>
   <elementGuidId>5e786986-a64f-4b4e-80eb-dd97edfeb27d</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>color</name>
      <type>Main</type>
      <value>primary</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>label-manager-main-view-assign-without-propagation-btn</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-raised-button mat-primary</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Assign
        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app modal-open&quot;]/ngb-modal-window[@class=&quot;modal fade label-manager-modal custom-modal show&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body ng-star-inserted&quot;]/div[@class=&quot;modal-body-content ng-star-inserted&quot;]/div[@class=&quot;modal-body-content-inner&quot;]/arn-label-manager-main-view[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;row mt-3&quot;]/div[@class=&quot;col-6 d-flex justify-content-end&quot;]/button[@id=&quot;label-manager-main-view-assign-without-propagation-btn&quot;]</value>
   </webElementProperties>
</WebElementEntity>
