<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_    group 1</name>
   <tag></tag>
   <elementGuidId>7d73c076-559c-40cb-a0ed-fe07d28866b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(contains(text(), 'group 1') or contains(., 'group 1'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>node-tree-item-wrapper</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>group 1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app modal-open&quot;]/ngb-modal-window[@class=&quot;modal fade label-manager-modal custom-modal show&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body ng-star-inserted&quot;]/div[@class=&quot;modal-body-content ng-star-inserted&quot;]/div[@class=&quot;modal-body-content-inner&quot;]/arn-label-manager-main-view[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-8&quot;]/div[@class=&quot;tree-container&quot;]/arn-label-manager-tree-container[1]/arn-node-tree-view[@class=&quot;label-manager-root-tree ng-star-inserted&quot;]/div[@class=&quot;node-tree-item-wrapper ng-star-inserted&quot;]</value>
   </webElementProperties>
</WebElementEntity>
