<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>13_button_previous</name>
   <tag></tag>
   <elementGuidId>b50c0ff2-f3e0-46d9-b821-e627a4bdbf0f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@tid = 'mt-projectReporting-button-previous']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-projectReporting-button-previous</value>
   </webElementProperties>
</WebElementEntity>
