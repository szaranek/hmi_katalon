<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mt-treeGrid-row-currentSpeed1CurrentDirection2-</name>
   <tag></tag>
   <elementGuidId>3c104e11-9342-4b77-8961-a9ec64b9f7ff</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-treeGrid-row-currentSpeed1CurrentDirection2-</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <value>//div[@id='mt-treeGrid-row-currentSpeed1CurrentDirection2-n28724']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//table[@id='DefaultView']/tbody/tr[3]/td/div/div/div/div/table/tbody/tr[3]/td/div/table/tbody/tr[7]/td/div/table/tbody/tr[3]/td/div/table/tbody/tr[4]/td/div/table/tbody/tr[2]/td[3]/div/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Current speed: %1, current direction: %2'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lang.CompassPoints'])[1]/preceding::div[17]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//tr[4]/td/div/table/tbody/tr[2]/td[3]/div/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
