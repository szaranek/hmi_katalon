<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mt-treeGrid-row-langMyConcat-</name>
   <tag></tag>
   <elementGuidId>b1a27b5e-f595-4226-92de-f447b54ccecf</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-treeGrid-row-langMyConcat-</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <value>//div[@id='mt-treeGrid-row-langMyConcat-g28717']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//table[@id='DefaultView']/tbody/tr[3]/td/div/div/div/div/table/tbody/tr[3]/td/div/table/tbody/tr[7]/td/div/table/tbody/tr[3]/td/div/table/tbody/tr[3]/td[3]/div/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lang.MyConcat'])[2]/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Some Infos'])[2]/following::div[9]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//tr[7]/td/div/table/tbody/tr[3]/td/div/table/tbody/tr[3]/td[3]/div/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
