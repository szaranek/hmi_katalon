<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CheckWidth</name>
   <tag></tag>
   <elementGuidId>44e6770b-41d4-43b2-84f1-874cbf499c1a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@tid, 'RichEditor-Area-div-Rows2/2-Chars17/50-Width129/222')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>RichEditor-Area-div-Rows2/2-Chars17/50-Width129/222</value>
   </webElementProperties>
</WebElementEntity>
