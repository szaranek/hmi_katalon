<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Width98            222</name>
   <tag></tag>
   <elementGuidId>f2d23e53-e3cf-4a24-9385-2c537a3c6d13</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>max-row</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    
        
            Width: 98
             / 222,
        
        Rows: 2
         / 2,
        Chars: 
            14
        
         / 50  
        
            Line Break: Manual
        
    
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app&quot;]/app-root[1]/div[@class=&quot;app has-header&quot;]/div[@class=&quot;content&quot;]/arn-test-gui[@class=&quot;ng-star-inserted&quot;]/app-dock-board[@class=&quot;has-toolbar ng-star-inserted&quot;]/app-dock[@class=&quot;mat-elevation-z4 ng-star-inserted&quot;]/div[@class=&quot;dock-component ng-star-inserted&quot;]/arn-mt-standard-text-dock-wrap[@class=&quot;ng-star-inserted&quot;]/arn-manual-translation-standard-text[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;mt-holder&quot;]/div[@class=&quot;col-12 content-content&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-6&quot;]/div[@class=&quot;form-group translation-container&quot;]/div[@class=&quot;translation-controls&quot;]/arn-rich-editor[@class=&quot;ng-star-inserted&quot;]/arn-area[1]/div[@class=&quot;max-row&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='•'])[3]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Last modified'])[1]/preceding::div[15]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[2]/div/div/arn-rich-editor/arn-area/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
