<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mt-labelManager-label-button-edit (1)</name>
   <tag></tag>
   <elementGuidId>ecdad5e6-ac5d-4b05-ac5a-057cca7eefeb</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-sm mat-icon-button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-labelManager-label-button-edit</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app modal-open&quot;]/ngb-modal-window[@class=&quot;modal fade label-manager-modal custom-modal show&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body ng-star-inserted&quot;]/div[@class=&quot;modal-body-content label-manager-modal-content ng-star-inserted&quot;]/div[@class=&quot;modal-body-content-inner&quot;]/arn-label-manager-main-view[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-8 y-scrollable&quot;]/div[@class=&quot;tree-container&quot;]/arn-label-manager-tree-container[1]/arn-node-tree-view[@class=&quot;label-manager-root-tree ng-star-inserted&quot;]/div[@class=&quot;node-tree-item-wrapper selected ng-star-inserted&quot;]/arn-label-manager-tree-label-view[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;label-node-wrapper row&quot;]/div[@class=&quot;col-2 text-right ng-star-inserted&quot;]/button[@class=&quot;mat-sm mat-icon-button&quot;]</value>
   </webElementProperties>
</WebElementEntity>
