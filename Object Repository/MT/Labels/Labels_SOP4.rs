<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Labels_SOP4</name>
   <tag></tag>
   <elementGuidId>09152e10-6ff5-459a-a078-ed982ed35451</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[starts-with(@id, 'mat-checkbox-') and @class = 'mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>mat-checkbox#mat-checkbox-28 label > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-labelManager-tree-label-SOP4</value>
   </webElementProperties>
</WebElementEntity>
