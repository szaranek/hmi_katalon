<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Labels_FutureTexts</name>
   <tag></tag>
   <elementGuidId>1804d0ab-882b-462e-8928-78cc430a7a41</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(contains(text(), 'FutureText') or contains(., 'FutureText'))]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ngb-modal-window:nth-child(12) > div > div > div.modal-body.ng-star-inserted > div > div > arn-label-manager-main-view > div:nth-child(2) > div.col-8 > div > arn-label-manager-tree-container > arn-node-tree-view > div.node-tree-item-wrapper.first.ng-star-inserted > i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>FutureText</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-labelManager-tree-group-label-FutureTexts</value>
   </webElementProperties>
</WebElementEntity>
