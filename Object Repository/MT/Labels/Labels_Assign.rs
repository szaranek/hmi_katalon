<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Labels_Assign</name>
   <tag></tag>
   <elementGuidId>7ed5fd48-bf8f-4a4d-9eab-5dec686ac0d1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ngb-modal-window:nth-child(12) > div > div > div.modal-body.ng-star-inserted > div > div > arn-label-manager-main-view > div.row.mt-3 > div.col-6.d-flex.justify-content-end > button#label-manager-main-view-assign-without-propagation-btn > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
