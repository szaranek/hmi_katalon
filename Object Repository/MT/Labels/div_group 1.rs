<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_group 1</name>
   <tag></tag>
   <elementGuidId>f8f6956c-ac8b-4e87-b17d-a2fb2bd05a61</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html[1]/body[@class=&quot;app modal-open&quot;]/ngb-modal-window[@class=&quot;modal fade label-manager-modal custom-modal show&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body ng-star-inserted&quot;]/div[@class=&quot;modal-body-content label-manager-modal-content ng-star-inserted&quot;]/div[@class=&quot;modal-body-content-inner&quot;]/arn-label-manager-main-view[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-8 y-scrollable&quot;]/div[@class=&quot;tree-container&quot;]/arn-label-manager-tree-container[1]/arn-node-tree-view[@class=&quot;label-manager-root-tree ng-star-inserted&quot;]/div[@class=&quot;node-tree-item-wrapper ng-star-inserted&quot;]/arn-label-manager-tree-group-view[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;group-node-wrapper&quot;][count(. | //div[@class = 'group-node-wrapper' and (contains(text(), 'group') or contains(., 'group'))]) = count(//div[@class = 'group-node-wrapper' and (contains(text(), 'group') or contains(., 'group'))])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>group-node-wrapper</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>group</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app modal-open&quot;]/ngb-modal-window[@class=&quot;modal fade label-manager-modal custom-modal show&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body ng-star-inserted&quot;]/div[@class=&quot;modal-body-content label-manager-modal-content ng-star-inserted&quot;]/div[@class=&quot;modal-body-content-inner&quot;]/arn-label-manager-main-view[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-8 y-scrollable&quot;]/div[@class=&quot;tree-container&quot;]/arn-label-manager-tree-container[1]/arn-node-tree-view[@class=&quot;label-manager-root-tree ng-star-inserted&quot;]/div[@class=&quot;node-tree-item-wrapper ng-star-inserted&quot;]/arn-label-manager-tree-group-view[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;group-node-wrapper&quot;]</value>
   </webElementProperties>
</WebElementEntity>
