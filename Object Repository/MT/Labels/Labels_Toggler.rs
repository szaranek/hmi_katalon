<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Labels_Toggler</name>
   <tag></tag>
   <elementGuidId>53163222-2f2d-4f3f-b320-a604af26653f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ngb-modal-window:nth-child(14) > div > div > div.modal-body.ng-star-inserted > div > div > arn-label-manager-main-view > div:nth-child(2) > div.col-8 > div > arn-label-manager-tree-container > arn-node-tree-view > div:nth-child(2) > i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app modal-open&quot;]/ngb-modal-window[@class=&quot;modal fade label-manager-modal custom-modal show&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body ng-star-inserted&quot;]/div[@class=&quot;modal-body-content ng-star-inserted&quot;]/div[@class=&quot;modal-body-content-inner&quot;]/arn-label-manager-main-view[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-8&quot;]/div[@class=&quot;tree-container&quot;]/arn-label-manager-tree-container[1]/arn-node-tree-view[@class=&quot;label-manager-root-tree ng-star-inserted&quot;]/div[@class=&quot;node-tree-item-wrapper ng-star-inserted&quot;]/i[@class=&quot;fa node-tree-expand-toggler fa-caret-right ng-star-inserted&quot;]</value>
   </webElementProperties>
</WebElementEntity>
