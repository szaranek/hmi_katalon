<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mt-translation-textarea-de-DE</name>
   <tag></tag>
   <elementGuidId>bf33dc13-3c7f-4449-bbee-5365e10f9bec</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-translation-textarea-de-DE</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Line $gid45211713540004344$ is not available.'])[1]/preceding::textarea[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[2]/div/div/arn-rich-editor/arn-area/div/textarea</value>
   </webElementXpaths>
</WebElementEntity>
