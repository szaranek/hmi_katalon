<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mat-pseudo-checkbox_mat-option</name>
   <tag></tag>
   <elementGuidId>4f8f8f93-5886-4205-bac4-ae1afd886af2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'mat-option-pseudo-checkbox mat-pseudo-checkbox ng-star-inserted']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>mat-pseudo-checkbox</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-option-pseudo-checkbox mat-pseudo-checkbox ng-star-inserted</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mat-option-101&quot;)/mat-pseudo-checkbox[@class=&quot;mat-option-pseudo-checkbox mat-pseudo-checkbox ng-star-inserted&quot;]</value>
   </webElementProperties>
</WebElementEntity>
