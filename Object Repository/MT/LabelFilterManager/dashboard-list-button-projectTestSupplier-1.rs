<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dashboard-list-button-projectTestSupplier-1</name>
   <tag></tag>
   <elementGuidId>25de3647-84ff-47b8-a09f-13aa28783942</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[starts-with(@tid, 'dashboard-list-button-projectTestSupplier-')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>dashboard-list-button-projectTestSupplier-</value>
   </webElementProperties>
</WebElementEntity>
