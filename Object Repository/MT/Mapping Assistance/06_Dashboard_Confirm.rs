<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>06_Dashboard_Confirm</name>
   <tag></tag>
   <elementGuidId>648a44a1-8a86-4691-a317-54ecb4b8b03f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@tid, 'button-open')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>button-open</value>
   </webElementProperties>
</WebElementEntity>
