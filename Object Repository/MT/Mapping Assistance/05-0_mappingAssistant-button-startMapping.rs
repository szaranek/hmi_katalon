<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>05-0_mappingAssistant-button-startMapping</name>
   <tag></tag>
   <elementGuidId>9c7d6394-0b38-4694-af43-e551024978a9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@tid, 'button-startMapping')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>button-startMapping</value>
   </webElementProperties>
</WebElementEntity>
