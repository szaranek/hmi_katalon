<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>layout-menu-button-userDashboard</name>
   <tag></tag>
   <elementGuidId>38f8d13a-c8bc-4a19-b6e4-249ff12aad9b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@tid = 'layout-menu-button-userDashboard']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#cdk-overlay-3 > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>layout-menu-button-userDashboard</value>
   </webElementProperties>
</WebElementEntity>
