<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>04_KAT_TEST</name>
   <tag></tag>
   <elementGuidId>4b1ee924-5e73-418d-9992-1ae91afa159c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//th[starts-with(@tid, 'KAT_TEST_PU')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>KAT_TEST_PU</value>
   </webElementProperties>
</WebElementEntity>
