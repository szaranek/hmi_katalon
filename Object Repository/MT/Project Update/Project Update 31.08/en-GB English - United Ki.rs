<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>en-GB English - United Ki</name>
   <tag></tag>
   <elementGuidId>689089aa-cb6c-41a8-8d44-93e650a53fbc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(contains(text(), 'en-GB: English - United Kingdom') or contains(., 'en-GB: English - United Kingdom'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>cnp-updateProject-languageNaming-option-en-GB: English - United Kingdom</value>
   </webElementProperties>
</WebElementEntity>
