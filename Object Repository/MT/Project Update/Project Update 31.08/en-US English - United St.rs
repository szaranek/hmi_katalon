<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>en-US English - United St</name>
   <tag></tag>
   <elementGuidId>d71a397c-1fe1-4c2d-bc10-8e576ee17acd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(contains(text(), 'en-US: English - United States') or contains(., 'en-US: English - United States'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>cnp-updateProject-languageNaming-option-en-US: English - United States</value>
   </webElementProperties>
</WebElementEntity>
