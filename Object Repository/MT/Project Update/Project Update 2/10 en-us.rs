<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>10 en-us</name>
   <tag></tag>
   <elementGuidId>e8fcbb4d-5365-48e9-9d90-e688545c21cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@placeholder = 'Select' and @id = 'mat-input-5']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>cnp-updateProject-languageNaming-input-en_US</value>
   </webElementProperties>
</WebElementEntity>
