<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>01_Edit_button</name>
   <tag></tag>
   <elementGuidId>75cc63d3-717d-41ce-b878-c1817e307e20</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@tid, 'dashboard-list-button-edit-')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>dashboard-list-button-edit-</value>
   </webElementProperties>
</WebElementEntity>
