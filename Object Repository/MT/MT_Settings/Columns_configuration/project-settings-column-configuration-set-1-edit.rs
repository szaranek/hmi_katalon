<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>project-settings-column-configuration-set-1-edit</name>
   <tag></tag>
   <elementGuidId>af997032-a216-4fef-b8e0-2950e05b2086</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@tid = 'project-settings-column-configuration-set-1-edit']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@tid, 'project-settings-column-configuration-set') and contains(@tid, 'edit') and not(contains(@tid, 'default'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>cdk-describedby-message-75</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>project-settings-column-configuration-set-1-edit</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                            edit
                                        
                                    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mat-tab-content-1-4&quot;)/div[@class=&quot;mat-tab-body-content ng-trigger ng-trigger-translateTab&quot;]/arn-project-settings-columns-configuration[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;content p-3&quot;]/div[@class=&quot;settings-columns-content&quot;]/div[@class=&quot;col&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;sets-container&quot;]/div[@class=&quot;set-record active ng-star-inserted&quot;]/div[2]/button[@class=&quot;mat-button&quot;]</value>
   </webElementProperties>
</WebElementEntity>
