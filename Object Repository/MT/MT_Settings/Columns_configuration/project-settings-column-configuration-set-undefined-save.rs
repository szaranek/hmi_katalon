<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>project-settings-column-configuration-set-undefined-save</name>
   <tag></tag>
   <elementGuidId>542b7d90-ea1a-49fe-8f72-5ea94c0ef82c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@tid = 'project-settings-column-configuration-set-undefined-save']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@tid, 'project-settings-column-configuration-set') and contains(@tid, 'save') and not(contains(@tid, 'default'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-button ng-star-inserted</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>cdk-describedby-message-79</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>project-settings-column-configuration-set-undefined-save</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                            save
                                        
                                    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mat-tab-content-1-4&quot;)/div[@class=&quot;mat-tab-body-content ng-trigger ng-trigger-translateTab&quot;]/arn-project-settings-columns-configuration[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;content p-3&quot;]/div[@class=&quot;settings-columns-content&quot;]/div[@class=&quot;col&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;sets-container&quot;]/div[@class=&quot;set-record active ng-star-inserted&quot;]/div[2]/button[@class=&quot;mat-button ng-star-inserted&quot;]</value>
   </webElementProperties>
</WebElementEntity>
