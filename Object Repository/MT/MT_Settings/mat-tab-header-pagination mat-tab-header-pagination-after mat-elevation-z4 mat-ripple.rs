<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mat-tab-header-pagination mat-tab-header-pagination-after mat-elevation-z4 mat-ripple</name>
   <tag></tag>
   <elementGuidId>997cfb63-4701-49ce-8dbc-3af6cf02d057</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@id, &quot;mat-dialog-&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'mat-tab-header-pagination mat-tab-header-pagination-after mat-elevation-z4 mat-ripple']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-tab-header-pagination mat-tab-header-pagination-after mat-elevation-z4 mat-ripple</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mat-dialog-1&quot;)/arn-project-settings-modal[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;modal-content&quot;]/mat-dialog-content[@class=&quot;modal-body mat-dialog-content&quot;]/mat-tab-group[@class=&quot;mat-tab-group mat-primary&quot;]/mat-tab-header[@class=&quot;mat-tab-header mat-tab-header-pagination-controls-enabled&quot;]/div[@class=&quot;mat-tab-header-pagination mat-tab-header-pagination-after mat-elevation-z4 mat-ripple&quot;]</value>
   </webElementProperties>
</WebElementEntity>
