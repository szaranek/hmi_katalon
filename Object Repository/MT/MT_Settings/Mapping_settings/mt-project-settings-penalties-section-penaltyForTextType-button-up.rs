<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mt-project-settings-penalties-section-penaltyForTextType-button-up</name>
   <tag></tag>
   <elementGuidId>ee6f5048-bbf7-48a8-a1ef-740c7d232a6f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-project-settings-penalties-section-penaltyForTextType-button-up</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;settings.penalty_for_text_type&quot;)/div[@class=&quot;input-group&quot;]/div[@class=&quot;input-group-btn-vertical&quot;]/button[@class=&quot;btn btn-default&quot;]</value>
   </webElementProperties>
</WebElementEntity>
