<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mt-standardText-button-saveAsCompletedAndNext</name>
   <tag></tag>
   <elementGuidId>5d5c41d3-e8f8-488b-b815-516faff87ad4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@tid = 'mt-standardText-button-saveAsCompletedAndNext']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-standardText-button-saveAsCompletedAndNext</value>
   </webElementProperties>
</WebElementEntity>
