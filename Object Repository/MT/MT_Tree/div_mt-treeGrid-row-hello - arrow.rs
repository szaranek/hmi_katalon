<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_mt-treeGrid-row-hello - arrow</name>
   <tag></tag>
   <elementGuidId>b54a8fc5-9609-46fc-9a25-d0e96de235fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[starts-with(@tid, 'mt-treeGrid-row-hello-')]/.././.././../td[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[starts-with(@tid, 'mt-treeGrid-row-hello')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-treeGrid-row-hello</value>
   </webElementProperties>
</WebElementEntity>
