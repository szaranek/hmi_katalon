<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_same-font ng-untouche</name>
   <tag></tag>
   <elementGuidId>3f6996a0-06a9-4169-bd2b-31a387bf458d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[starts-with(@tid, 'mt-translation-textarea-de-DE')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-translation-textarea-de-DE</value>
   </webElementProperties>
</WebElementEntity>
