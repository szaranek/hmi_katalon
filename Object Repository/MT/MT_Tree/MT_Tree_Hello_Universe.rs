<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MT_Tree_Hello_Universe</name>
   <tag></tag>
   <elementGuidId>21f699a2-c19a-4da5-87c3-43057239c3ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[starts-with(@tid, 'mt-treeGrid-row-helloUniverse-')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-treeGrid-row-helloUniverse-</value>
   </webElementProperties>
</WebElementEntity>
