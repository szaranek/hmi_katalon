<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_arn-icon arn-icon-save-yello</name>
   <tag></tag>
   <elementGuidId>aa080cd4-be60-459f-a9f2-8415a6e2551f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'arn-icon arn-icon-save-yellow']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>arn-icon arn-icon-save-yellow</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app&quot;]/app-root[1]/div[@class=&quot;app has-header&quot;]/div[@class=&quot;content&quot;]/arn-test-gui[@class=&quot;ng-star-inserted&quot;]/app-dock-board[@class=&quot;has-toolbar&quot;]/app-dock[@class=&quot;mat-elevation-z4 ng-star-inserted&quot;]/div[@class=&quot;dock-component ng-star-inserted&quot;]/arn-mt-standard-text-dock-wrap[@class=&quot;ng-star-inserted&quot;]/arn-manual-translation-standard-text[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;col-12 content-content&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-6&quot;]/div[@class=&quot;form-group translation-container&quot;]/div[@class=&quot;translation-controls&quot;]/div[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;pull-right&quot;]/button[@class=&quot;mat-md save-button mat-icon-button&quot;]/span[@class=&quot;mat-button-wrapper&quot;]/i[@class=&quot;arn-icon arn-icon-save-yellow&quot;]</value>
   </webElementProperties>
</WebElementEntity>
