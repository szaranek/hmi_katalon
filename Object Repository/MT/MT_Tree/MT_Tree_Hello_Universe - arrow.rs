<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MT_Tree_Hello_Universe - arrow</name>
   <tag></tag>
   <elementGuidId>fe07fd66-df74-4142-a0b1-a347c45eec9a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[starts-with(@tid, 'mt-treeGrid-row-helloUniverse')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[starts-with(@tid, 'mt-treeGrid-row-helloUniverse-')]/.././.././../td[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-treeGrid-row-helloUniverse</value>
   </webElementProperties>
</WebElementEntity>
