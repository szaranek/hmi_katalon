<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>STC_List_Root_Group_test</name>
   <tag></tag>
   <elementGuidId>2109b87d-e080-4fab-b050-7aec3a4c1ac2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value> //*[starts-with(@id, 'stc-treeGrid-row-group-test')]/parent::td/parent::tr</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[starts-with(@tid, 'stc-treeGrid-row-group-test')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>stc-treeGrid-row-group</value>
   </webElementProperties>
</WebElementEntity>
