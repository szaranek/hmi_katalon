<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>STC_List_PopupAddRootGroup_Yes</name>
   <tag></tag>
   <elementGuidId>fe552266-a8a5-43aa-9670-1ee6750dac93</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@tid = 'stc-list-popupAddRootGroup-button-yes']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>stc-list-popupAddRootGroup-button-yes</value>
   </webElementProperties>
</WebElementEntity>
