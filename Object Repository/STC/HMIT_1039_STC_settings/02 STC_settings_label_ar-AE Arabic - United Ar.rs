<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>02 STC_settings_label_ar-AE Arabic - United Ar</name>
   <tag></tag>
   <elementGuidId>27022a32-9e0a-4dc6-ac30-001402a99d23</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>custom-control custom-checkbox d-inline-block</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>stc-settings-modal-select-ar-AE: Arabic - United Arab Emirates</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        
                                                        
                                                        ar-AE: Arabic - United Arab Emirates
                                                    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app modal-open&quot;]/ngb-modal-window[@class=&quot;modal fade show custom-modal&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/app-sample-catalog-settings[1]/div[@class=&quot;material-version&quot;]/div[@class=&quot;container-wrap&quot;]/div[@class=&quot;body-container&quot;]/div[@class=&quot;mb-4&quot;]/div[@class=&quot;sample-catalog-settings&quot;]/div[@class=&quot;content p-2&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;row ng-star-inserted&quot;]/div[@class=&quot;col&quot;]/div[@class=&quot;language-container&quot;]/div[@class=&quot;ng-star-inserted&quot;]/label[@class=&quot;custom-control custom-checkbox d-inline-block&quot;]</value>
   </webElementProperties>
</WebElementEntity>
