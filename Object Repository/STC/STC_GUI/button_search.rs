<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_search</name>
   <tag></tag>
   <elementGuidId>ba3c2cd0-0681-4c94-9afe-26c06c671f4c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[(contains(text(), 'Search') or contains(., 'Search'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>color</name>
      <type>Main</type>
      <value>primary</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-raised-button mat-primary</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Search</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app&quot;]/app-root[1]/div[@class=&quot;app has-header&quot;]/div[@class=&quot;content&quot;]/arn-sample-catalog[@class=&quot;ng-star-inserted&quot;]/app-dock-board[@class=&quot;ng-star-inserted&quot;]/app-dock[@class=&quot;mat-elevation-z4 ng-star-inserted&quot;]/div[@class=&quot;dock-component ng-star-inserted&quot;]/arn-search-dock-wrap[@class=&quot;ng-star-inserted&quot;]/app-sample-catalog-search[1]/arn-search[1]/div[@class=&quot;opened&quot;]/footer[@class=&quot;ng-star-inserted&quot;]/div[2]/button[@class=&quot;mat-raised-button mat-primary&quot;]</value>
   </webElementProperties>
</WebElementEntity>
