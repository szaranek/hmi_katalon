<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>STC_GUI_Presets_search</name>
   <tag></tag>
   <elementGuidId>09cb139c-9a27-44b6-80e8-9d691fe88dfa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@tid = 'stc-gui-menu-button-windowConfiguration-Search-Preset']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>stc-gui-menu-button-windowConfiguration-Search-Preset</value>
   </webElementProperties>
</WebElementEntity>
