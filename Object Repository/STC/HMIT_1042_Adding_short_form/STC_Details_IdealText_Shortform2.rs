<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>STC_Details_IdealText_Shortform2</name>
   <tag></tag>
   <elementGuidId>129bf3d8-b4bc-4051-aa76-1182c376f4e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@tid, 'mt-translation-textarea-stc-details-idealText-textarea-2')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-translation-textarea-stc-details-idealText-textarea-2</value>
   </webElementProperties>
</WebElementEntity>
