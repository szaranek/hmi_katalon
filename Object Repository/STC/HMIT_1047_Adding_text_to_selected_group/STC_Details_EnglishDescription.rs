<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>STC_Details_EnglishDescription</name>
   <tag></tag>
   <elementGuidId>f40cefe3-56b2-409b-afbf-e1e017e1607c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@tid = 'mt-translation-textarea-stc-details-description-textarea-english']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-translation-textarea-stc-details-description-textarea-english</value>
   </webElementProperties>
</WebElementEntity>
