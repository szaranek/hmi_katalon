<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Save - shortform1</name>
   <tag></tag>
   <elementGuidId>4c0e62d1-757e-4f75-9fc0-d958d1b4b34f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;text1-panel&quot;)/div[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;pull-right&quot;]/button[@class=&quot;mat-md save-button mat-icon-button&quot;]/span[@class=&quot;mat-button-wrapper&quot;]/i[@class=&quot;arn-icon arn-icon-save-green&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>stc-details-stc-details-button-saveAsDone-1</value>
   </webElementProperties>
</WebElementEntity>
