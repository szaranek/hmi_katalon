<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Save - shortform2</name>
   <tag></tag>
   <elementGuidId>271eca63-74cf-4e5c-9f4f-c22cf8142f85</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;text2-panel&quot;)/div[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;pull-right&quot;]/button[@class=&quot;mat-md save-button mat-icon-button&quot;]/span[@class=&quot;mat-button-wrapper&quot;]/i[@class=&quot;arn-icon arn-icon-save-green&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>stc-details-stc-details-button-saveAsDone-2</value>
   </webElementProperties>
</WebElementEntity>
