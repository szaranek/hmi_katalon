<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Save </name>
   <tag></tag>
   <elementGuidId>c80f9d92-9fab-463b-b9a6-9cef599c06fc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;text0-panel&quot;)/div[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;pull-right&quot;]/button[@class=&quot;mat-md save-button mat-icon-button&quot;]/span[@class=&quot;mat-button-wrapper&quot;]/i[@class=&quot;arn-icon arn-icon-save-green&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>stc-details-stc-details-button-saveAsDone-0</value>
   </webElementProperties>
</WebElementEntity>
