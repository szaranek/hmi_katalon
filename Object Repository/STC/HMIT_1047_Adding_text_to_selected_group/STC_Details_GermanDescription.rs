<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>STC_Details_GermanDescription</name>
   <tag></tag>
   <elementGuidId>3236fdf9-522a-41d6-a58f-f8bdb949ba21</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@tid = 'mt-translation-textarea-stc-details-description-textarea-german']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>mt-translation-textarea-stc-details-description-textarea-german</value>
   </webElementProperties>
</WebElementEntity>
