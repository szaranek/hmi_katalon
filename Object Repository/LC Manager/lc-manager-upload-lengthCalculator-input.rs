<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lc-manager-upload-lengthCalculator-input</name>
   <tag></tag>
   <elementGuidId>7372ba8e-eafa-4f8e-9eec-3384b03b1b7a</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tid</name>
      <type>Main</type>
      <value>lc-manager-upload-lengthCalculator-input</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='cdk-overlay-6']/ngb-modal-window/div/div/div[2]/div/div[2]/div/label</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select file:'])[1]/following::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[2]/div/label</value>
   </webElementXpaths>
</WebElementEntity>
