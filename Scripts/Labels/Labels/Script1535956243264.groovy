import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/button_label'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/button_New group'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/div_group 1'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/mt-labelManager-group-button-edit'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/Labels/mt-labelManager-group-name-input'), 'KatalonEdit')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/mt-labelManager-group-name-button-update'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/mt-labelManager-group-button-delete'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/mt-labelManager-button-newLabelWithoutGroup'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/Labels/mt-labelManager-label-form-name'), 'KatalonLabel')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/mt-labelManager-label-form-bugfix'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/mt-labelManager-label-form-restriction-to-lowercase'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/mt-labelManager-label-form-restriction-to-uppercase'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/mt-labelManager-label-form-is-re'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/mt-labelManager-label-form-button-save'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/span_KatalonLabel'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/mt-labelManager-label-button-edit'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/mt-labelManager-label-form-button-cancel'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/mt-labelManager-label-button-delete'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Labels/labelManagerClose'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.closeBrowser()

