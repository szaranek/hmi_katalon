import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://145.239.90.33/login')

WebUI.setText(findTestObject('LogIn/00 HMIT_1059_Log_in_to_HMI_tool/input_username'), 'TEST_ADMIN')

WebUI.setText(findTestObject('LogIn/00 HMIT_1059_Log_in_to_HMI_tool/input_password'), 'abc')

WebUI.click(findTestObject('LogIn/00 HMIT_1059_Log_in_to_HMI_tool/button_Sign In'))

WebUI.verifyElementPresent(findTestObject('LogIn/01 HMIT_1060_Failed_log_in_to_HMI_tool/div_Username or password is in'), 3)

WebUI.setText(findTestObject('LogIn/00 HMIT_1059_Log_in_to_HMI_tool/input_username'), 'abc')

WebUI.setText(findTestObject('LogIn/00 HMIT_1059_Log_in_to_HMI_tool/input_password'), 'TEST')

WebUI.click(findTestObject('LogIn/00 HMIT_1059_Log_in_to_HMI_tool/button_Sign In'))

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('LogIn/01 HMIT_1060_Failed_log_in_to_HMI_tool/div_Username or password is in'), 3)

