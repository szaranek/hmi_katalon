import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(null)

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.URL)

WebUI.clearText(findTestObject('LogIn/00 HMIT_1059_Log_in_to_HMI_tool/input_username'))

WebUI.setText(findTestObject('LogIn/00 HMIT_1059_Log_in_to_HMI_tool/input_username'), 'TEST_ADMIN')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('LogIn/00 HMIT_1059_Log_in_to_HMI_tool/input_password'), 'TEST')

WebUI.clearText(findTestObject('LogIn/00 HMIT_1059_Log_in_to_HMI_tool/input_password'))

WebUI.setText(findTestObject('LogIn/00 HMIT_1059_Log_in_to_HMI_tool/input_password'), 'TEST')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('LogIn/00 HMIT_1059_Log_in_to_HMI_tool/button_Sign In'))

WebUI.waitForElementVisible(findTestObject('STC/Menu'), 4)

