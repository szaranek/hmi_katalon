import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import tools.RandomString as RandomString

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Help System/Add Root Group/helpSystem-addRootGroup'))

WebUI.delay(1)

WebUI.takeScreenshot()

GlobalVariable.HelpSystemName = ('TestGroup' + RandomString.randomString())

WebUI.setText(findTestObject('Object Repository/Help System/Add Root Group/helpSystem-saveModal-input'), GlobalVariable.HelpSystemName)

GlobalVariable.HSgroup = WebUI.modifyObjectProperty(GlobalVariable.HSgroup, 'tid', 'starts with', 'helpSystem-toggleGroup-' + 
    GlobalVariable.HelpSystemName, true)

GlobalVariable.HSaddsub = WebUI.modifyObjectProperty(GlobalVariable.HSaddsub, 'tid', 'starts with', 'helpSystem-addSubgroup-' + 
    GlobalVariable.HelpSystemName, true)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Help System/Add Root Group/helpSystem-saveModal-saveButton'))

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(GlobalVariable.HSgroup, 3)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(GlobalVariable.HSgroup)

WebUI.delay(1)

WebUI.takeScreenshot()

