import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import tools.RandomString as RandomString

WebUI.click(GlobalVariable.HSpage)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToPosition(0, 0)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Help System/EditDelete Page/helpSystem-renameTitle'))

WebUI.delay(1)

WebUI.takeScreenshot()

GlobalVariable.HelpSystemName = ('EditedTestPage' + RandomString.randomString())

GlobalVariable.HSpage = WebUI.modifyObjectProperty(GlobalVariable.HSpage, 'tid', 'starts with', 'helpSystem-deleteInstruction-' + 
    GlobalVariable.HelpSystemName, true)

WebUI.setText(findTestObject('Object Repository/Help System/EditDelete Page/helpSystem-saveModal-input'), GlobalVariable.HelpSystemName)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Help System/EditDelete Page/helpSystem-saveModal-saveButton'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyTextPresent(GlobalVariable.HelpSystemName, false)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(GlobalVariable.HSpage)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Help System/EditDelete Page/helpSystem-deleteInstruction'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementNotPresent(GlobalVariable.HSpage, 
    5)

WebUI.closeBrowser()

