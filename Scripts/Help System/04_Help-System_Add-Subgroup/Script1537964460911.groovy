import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import tools.RandomString as RandomString

WebUI.click(GlobalVariable.HSaddsub)

WebUI.delay(1)

WebUI.takeScreenshot()

GlobalVariable.HelpSystemName = ('TestSubgroup' + RandomString.randomString())

WebUI.setText(findTestObject('Object Repository/Help System/Adding Subgroup/helpSystem-saveModal-input'), GlobalVariable.HelpSystemName)

WebUI.delay(1)

WebUI.takeScreenshot()

GlobalVariable.HSsubgroup = WebUI.modifyObjectProperty(GlobalVariable.HSsubgroup, 'tid', 'starts with', 'helpSystem-toggleGroup-' + 
    GlobalVariable.HelpSystemName, true)

GlobalVariable.HSaddpage = WebUI.modifyObjectProperty(GlobalVariable.HSaddpage, 'tid', 'starts with', 'helpSystem-addPage-' + 
    GlobalVariable.HelpSystemName, true)

WebUI.click(findTestObject('Object Repository/Help System/Adding Subgroup/helpSystem-saveModal-saveButton'))

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(GlobalVariable.HSsubgroup, 3)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToPosition(0, 800)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.clickOffset(GlobalVariable.HSsubgroup,0,0)

WebUI.delay(1)

WebUI.takeScreenshot()

