import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.waitForPageLoad(10)

WebUI.delay(5)

WebUI.click(findTestObject('Unicode/mt-translation-textarea-en'))

WebUI.setText(findTestObject('Unicode/mt-translation-textarea-en'), 'stop')

WebUI.click(findTestObject('Unicode/unicode-tab'))

WebUI.verifyElementPresent(findTestObject('Unicode/shared-unicode-characterSelect-0'), 5)

WebUI.verifyElementPresent(findTestObject('Unicode/shared-unicode-characterSelect-1'), 5)

WebUI.verifyElementPresent(findTestObject('Unicode/shared-unicode-characterSelect-2'), 5)

WebUI.verifyElementPresent(findTestObject('Unicode/shared-unicode-characterSelect-3'), 5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Unicode/shared-unicode-characterSelect-0'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Unicode/mt-translation-textarea-en'), Keys.chord(Keys.DELETE))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.sendKeys(findTestObject('Unicode/mt-translation-textarea-en'), Keys.chord(Keys.DELETE))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.sendKeys(findTestObject('Unicode/mt-translation-textarea-en'), Keys.chord(Keys.DELETE))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.sendKeys(findTestObject('Unicode/mt-translation-textarea-en'), Keys.chord(Keys.DELETE))

WebUI.verifyElementNotPresent(findTestObject('Unicode/shared-unicode-characterSelect-0'), 5)

WebUI.verifyElementNotPresent(findTestObject('Unicode/shared-unicode-characterSelect-1'), 5)

WebUI.verifyElementNotPresent(findTestObject('Unicode/shared-unicode-characterSelect-2'), 5)

WebUI.verifyElementNotPresent(findTestObject('Unicode/shared-unicode-characterSelect-3'), 5)

WebUI.takeScreenshot()

WebUI.closeBrowser()

