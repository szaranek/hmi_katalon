import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.delay(5)

WebUI.click(findTestObject('MT/LabelFilterManager/mat-icon_filter_list'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/LabelFilterManager/mt-filterLabelsManager-form-filterName'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/LabelFilterManager/mt-filterLabelsManager-form-filterName'), 'TestFilter')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/LabelFilterManager/mt-filterLabelsManager-form-filterQuery'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/LabelFilterManager/mt-filterLabelsManager-form-filterQuery'), ' label="Std"')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/LabelFilterManager/mt-filterLabelsManager-form-button-save'))

WebUI.delay(1)

WebUI.takeScreenshot()

//WebUI.click(findTestObject('MT/LabelFilterManager/button_play_circle_outline'))
//
//WebUI.delay(1)
//
//WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/LabelFilterManager/mat-icon_edit'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/LabelFilterManager/mt-filterLabelsManager-form-filterName'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/LabelFilterManager/mt-filterLabelsManager-form-filterName'), 'TestFilterEdit')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/LabelFilterManager/mt-filterLabelsManager-form-button-save'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/LabelFilterManager/mt-filterLabelsManager-modal-button-close'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('MT/LabelFilterManager/div_mt-treeGrid-row-helloWorld'), 0)

WebUI.verifyElementNotPresent(findTestObject('MT/LabelFilterManager/div_mt-treeGrid-row-helloUnive'), 0)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/LabelFilterManager/mat-icon_filter_list'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/LabelFilterManager/mat-icon_delete'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/LabelFilterManager/mt-filterLabelsManager-modal-button-close'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('MT/LabelFilterManager/div_mt-treeGrid-row-helloWorld'), 0)

WebUI.verifyElementPresent(findTestObject('MT/LabelFilterManager/div_mt-treeGrid-row-helloUnive'), 0)

WebUI.closeBrowser()

