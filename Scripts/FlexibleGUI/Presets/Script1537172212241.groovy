import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('FlexibleGUI/Presets/stc-menu-button-gui'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.mouseOver(findTestObject('FlexibleGUI/Presets/stc-gui-menu-button-presets'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('FlexibleGUI/Presets/stc-gui-menu-button-presets-ManualTranslation'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('FlexibleGUI/Presets/node-table-tab'), 5)

WebUI.verifyElementPresent(findTestObject('FlexibleGUI/Presets/translation-h3-title'), 5)

WebUI.verifyElementPresent(findTestObject('FlexibleGUI/Presets/tree-h3-title'), 5)

WebUI.verifyElementPresent(findTestObject('FlexibleGUI/Presets/unicode-h3-title'), 5)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('FlexibleGUI/Presets/stc-menu-button-gui'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.mouseOver(findTestObject('FlexibleGUI/Presets/stc-gui-menu-button-presets'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('FlexibleGUI/Presets/stc-gui-menu-button-presets-Simpletranslation'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('FlexibleGUI/Presets/tree-h3-title'), 5)

WebUI.verifyElementPresent(findTestObject('FlexibleGUI/Presets/translation-h3-title'), 5)

WebUI.verifyElementPresent(findTestObject('FlexibleGUI/Presets/tree-h3-title'), 5)

WebUI.verifyElementNotPresent(findTestObject('FlexibleGUI/Presets/unicode-h3-title'), 5)

WebUI.verifyElementNotPresent(findTestObject('FlexibleGUI/Presets/node-table-tab'), 5)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('FlexibleGUI/Presets/stc-menu-button-gui'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.mouseOver(findTestObject('FlexibleGUI/Presets/stc-gui-menu-button-presets'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('FlexibleGUI/Presets/stc-gui-menu-button-presets-Screenreview'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('FlexibleGUI/Presets/tree-h3-title'), 5)

WebUI.verifyElementPresent(findTestObject('FlexibleGUI/Presets/screen-h3-title'), 5)

WebUI.verifyElementNotPresent(findTestObject('FlexibleGUI/Presets/unicode-h3-title'), 5)

WebUI.verifyElementNotPresent(findTestObject('FlexibleGUI/Presets/translation-h3-title'), 5)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('FlexibleGUI/Presets/stc-menu-button-gui'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.mouseOver(findTestObject('FlexibleGUI/Presets/stc-gui-menu-button-presets'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('FlexibleGUI/Presets/stc-gui-menu-button-windowConfiguration-Properties'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('FlexibleGUI/Presets/tree-h3-title'), 5)

WebUI.verifyElementPresent(findTestObject('FlexibleGUI/Presets/properties-h3-title'), 5)

WebUI.verifyElementNotPresent(findTestObject('FlexibleGUI/Presets/screen-h3-title'), 5)

WebUI.verifyElementNotPresent(findTestObject('FlexibleGUI/Presets/unicode-h3-title'), 5)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('FlexibleGUI/Presets/stc-menu-button-gui'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.mouseOver(findTestObject('FlexibleGUI/Presets/stc-gui-menu-button-presets'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('FlexibleGUI/Presets/stc-gui-menu-button-presets-Search'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('FlexibleGUI/Presets/search-h3-title'), 5)

WebUI.verifyElementNotPresent(findTestObject('FlexibleGUI/Presets/properties-h3-title'), 5)

WebUI.verifyElementNotPresent(findTestObject('FlexibleGUI/Presets/screen-h3-title'), 5)

WebUI.verifyElementNotPresent(findTestObject('FlexibleGUI/Presets/unicode-h3-title'), 5)

WebUI.closeBrowser()

