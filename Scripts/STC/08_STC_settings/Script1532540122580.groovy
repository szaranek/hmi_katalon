import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.click(findTestObject('STC/HMIT_1039_STC_settings/STC-list-settings-button-open'))

WebUI.delay(2)

WebUI.click(findTestObject('STC/HMIT_1039_STC_settings/00 STC_settings_label_af-ZA Afrikaans - South'))

WebUI.click(findTestObject('STC/HMIT_1039_STC_settings/STC_settings_arrow_right'))

WebUI.click(findTestObject('STC/HMIT_1039_STC_settings/02 STC_settings_label_ar-AE Arabic - United Ar'))

WebUI.click(findTestObject('STC/HMIT_1039_STC_settings/STC_settings_arrow_right'))

WebUI.delay(2)

WebUI.click(findTestObject('STC/HMIT_1039_STC_settings/01 selected STC_settings_label_af-ZA Afrikaans - South - Copy'))

//WebUI.click(findTestObject('STC/HMIT_1039_STC_settings/STC_settings_label_en-GB English - United K'))
WebUI.click(findTestObject('STC/HMIT_1039_STC_settings/STC_settings_arrow_left'))

WebUI.click(findTestObject('STC/HMIT_1039_STC_settings/03 selected STC_settings_label_ar-AE Arabic - United Ar - Copy'))

//WebUI.click(findTestObject('STC/HMIT_1039_STC_settings/STC_settings_label_it-IT Italian - Italy'))
WebUI.click(findTestObject('STC/HMIT_1039_STC_settings/STC_settings_arrow_left'))

WebUI.delay(2)

WebUI.click(findTestObject('STC/STC_GUI/button_Submit'))

WebUI.closeBrowser()

