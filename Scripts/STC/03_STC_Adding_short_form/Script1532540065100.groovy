import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import org.junit.After
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement as WebElement

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.doubleClick(GlobalVariable.Translation)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('STC/HMIT_1042_Adding_short_form/STC_Details_AddShortform'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('STC/HMIT_1042_Adding_short_form/STC_Details_IdealText_Shortform1'), 2)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('STC/HMIT_1042_Adding_short_form/STC_Details_IdealText_Shortform1'), 'testTransla')

WebUI.scrollToElement(findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/Repair Patch 21.06.2018/Save - shortform1'), 
    5)

WebUI.click(findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/Repair Patch 21.06.2018/Save - shortform1'))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('Object Repository/STC/HMIT_1042_Adding_short_form/shortform1'), 5)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/STC/HMIT_1042_Adding_short_form/shortform1'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('STC/HMIT_1042_Adding_short_form/STC_Details_AddShortform'), 5)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('STC/HMIT_1042_Adding_short_form/STC_Details_AddShortform'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('STC/HMIT_1042_Adding_short_form/STC_Details_IdealText_Shortform2'), 5)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('STC/HMIT_1042_Adding_short_form/STC_Details_IdealText_Shortform2'), 'testTrans')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/Repair Patch 21.06.2018/Save - shortform2'), 
    5)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/Repair Patch 21.06.2018/Save - shortform2'))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('Object Repository/STC/HMIT_1042_Adding_short_form/shortform2'), 5)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/STC/HMIT_1042_Adding_short_form/shortform2'))

WebUI.delay(1)

WebUI.takeScreenshot()

