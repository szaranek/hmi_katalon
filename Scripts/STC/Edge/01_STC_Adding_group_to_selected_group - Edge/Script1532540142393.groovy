import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import tools.RandomString as RandomString
import org.openqa.selenium.Keys as Keys

WebUI.delay(1)

WebUI.doubleClick(findTestObject('STC/HMIT-1049_Adding_main_group/STC_List_Add'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('STC/HMIT_1048_Adding_group_to_selected_group/STC_List_Add_Group'))

WebUI.delay(1)

WebUI.takeScreenshot()

GlobalVariable.SubGroupName = ('testSub' + RandomString.randomString())

WebUI.setText(findTestObject('Object Repository/STC/HMIT_1048_Adding_group_to_selected_group/STC_List_Add_Sub_input-name'), GlobalVariable.SubGroupName)

GlobalVariable.SubGroup = WebUI.modifyObjectProperty(GlobalVariable.SubGroup, 'tid', 'starts with', 'stc-treeGrid-row-group-' +
	GlobalVariable.SubGroupName, true)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('STC/HMIT_1048_Adding_group_to_selected_group/STC_List_Add_Sub_button_ADD'))

WebUI.takeScreenshot()

//WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ARROW_RIGHT))

WebUI.delay(1)

WebUI.verifyElementPresent(GlobalVariable.SubGroup, 5)

WebUI.takeScreenshot()