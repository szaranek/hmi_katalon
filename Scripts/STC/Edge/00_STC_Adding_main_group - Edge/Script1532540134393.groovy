import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import net.bytebuddy.utility.RandomString as RandomString
import tools.RandomString as RandomString
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement as WebElement

WebUI.waitForElementVisible(findTestObject('STC/Menu_GUI'), 1)

WebUI.delay(2)

WebUI.doubleClick(findTestObject('STC/HMIT-1049_Adding_main_group/STC_List_Add'))

WebUI.delay(1)

WebUI.doubleClick(findTestObject('STC/HMIT-1049_Adding_main_group/STC_List_Add_Root'))

GlobalVariable.GroupName = ('test' + RandomString.randomString())

WebUI.setText(findTestObject('STC/HMIT-1049_Adding_main_group/STC_List_Add_Root_input-name'), GlobalVariable.GroupName)

GlobalVariable.Group = WebUI.modifyObjectProperty(GlobalVariable.Group, 'tid', 'starts with', 'stc-treeGrid-row-group-' + 
    GlobalVariable.GroupName, true)

WebUI.doubleClick(findTestObject('STC/HMIT-1049_Adding_main_group/STC_List_PopupAddRootGroup_Yes'))

WebUI.delay(2)

WebUI.verifyElementPresent(GlobalVariable.Group, 5)

WebUI.takeScreenshot()
//WebUI.focus(findTestObject('STC/HMIT-1049_Adding_main_group/STC_List_Root_Group_test'))
WebUI.delay(2)

WebUI.doubleClick(GlobalVariable.Group)

//WebUI.clickOffset(findTestObject('STC/HMIT-1049_Adding_main_group/STC_List_Root_Group_test'), 20, 20)
WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ARROW_RIGHT))

WebUI.delay(1)

WebUI.takeScreenshot()

