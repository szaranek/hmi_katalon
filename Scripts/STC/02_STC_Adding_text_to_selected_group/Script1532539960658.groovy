import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import tools.RandomString as RandomString

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('STC/HMIT-1049_Adding_main_group/STC_List_Add'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/STC_List_Add_Translation'))

//
GlobalVariable.TranslationName = ('testTranslation' + RandomString.randomString())

WebUI.scrollToElement(findTestObject('Object Repository/STC/HMIT_1047_Adding_text_to_selected_group/STC_Details_IdealText'), 
    5)

WebUI.setText(findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/STC_Details_IdealText'), GlobalVariable.TranslationName)

GlobalVariable.Translation = WebUI.modifyObjectProperty(GlobalVariable.Translation, 'tid', 'starts with', 'stc-treeGrid-row-mainText-' + 
    GlobalVariable.TranslationName, true)

//
WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('Object Repository/STC/HMIT_1047_Adding_text_to_selected_group/mat-expansion-panel-header_Con'), 
    5)

WebUI.click(findTestObject('Object Repository/STC/HMIT_1047_Adding_text_to_selected_group/mat-expansion-panel-header_Con'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/STC_Details_EnglishDescription'), 5)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/STC_Details_EnglishDescription'), 'testEnglishDescription')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/STC_Details_GermanDescription'), 5)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/STC_Details_GermanDescription'), 'testGermanDescription')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('Object Repository/STC/HMIT_1047_Adding_text_to_selected_group/Repair Patch 21.06.2018/Gender'), 
    5)

WebUI.click(findTestObject('Object Repository/STC/HMIT_1047_Adding_text_to_selected_group/Repair Patch 21.06.2018/Gender'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/STC/HMIT_1047_Adding_text_to_selected_group/Repair Patch 21.06.2018/Gender-Neutral'))

WebUI.clickOffset(findTestObject('Object Repository/STC/HMIT_1047_Adding_text_to_selected_group/Repair Patch 21.06.2018/Numerous'), 
    0, -20, FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Object Repository/STC/HMIT_1047_Adding_text_to_selected_group/Repair Patch 21.06.2018/Numerous'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/STC/HMIT_1047_Adding_text_to_selected_group/Repair Patch 21.06.2018/Numerous-Plural'))

WebUI.clickOffset(findTestObject('Object Repository/STC/HMIT_1047_Adding_text_to_selected_group/Repair Patch 21.06.2018/Gender'), 
    0, 20, FailureHandling.OPTIONAL)

WebUI.scrollToElement(findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/Repair Patch 21.06.2018/Save '), 5)

WebUI.click(findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/Repair Patch 21.06.2018/Save '))

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.scrollToPosition(0, 0)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(GlobalVariable.Translation, 5)

WebUI.takeScreenshot()

