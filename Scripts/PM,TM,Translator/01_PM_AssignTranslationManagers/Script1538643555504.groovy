import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('PM,TM,Translator/00_LogInToPM'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.takeScreenshot()

GlobalVariable.TRSubProjectName = GlobalVariable.TRSubProjectName.toString().replace('k', 'K')

GlobalVariable.TRexpand = WebUI.modifyObjectProperty(GlobalVariable.TRexpand, 'tid', 'contains', 'expand-' + GlobalVariable.TRSubProjectName, 
    true)

GlobalVariable.TRSubProject = WebUI.modifyObjectProperty(GlobalVariable.TRSubProject, 'tid', 'starts with', ('dashboard-list-button-' + 
    GlobalVariable.TRSubProjectName) + '-subProjectMenu', true)

GlobalVariable.TRenus = WebUI.modifyObjectProperty(GlobalVariable.TRenus, 'tid', 'starts with', ('dashboard-list-button-menu-' + 
    GlobalVariable.TRSubProjectName) + '-projectReadyMenu-en-US-', true)

GlobalVariable.TRengb = WebUI.modifyObjectProperty(GlobalVariable.TRenus, 'tid', 'starts with', ('dashboard-list-button-menu-' + 
    GlobalVariable.TRSubProjectName) + '-projectReadyMenu-en-GB-', true)

GlobalVariable.TRdeat = WebUI.modifyObjectProperty(GlobalVariable.TRenus, 'tid', 'starts with', ('dashboard-list-button-menu-' + 
    GlobalVariable.TRSubProjectName) + '-projectReadyMenu-de-AT-', true)

WebUI.scrollToElement(GlobalVariable.TRSubProject, 3)

WebUI.click(GlobalVariable.TRSubProject)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/PM,TM,Translator flow/PM/button_Assign Translation Mana'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/PM,TM,Translator flow/PM/input-de-AT'))

WebUI.click(findTestObject('Object Repository/PM,TM,Translator flow/PM/option-FRODO_BAGGINS'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/PM,TM,Translator flow/PM/input-en-GB'))

WebUI.click(findTestObject('Object Repository/PM,TM,Translator flow/PM/option-FRODO_BAGGINS'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/PM,TM,Translator flow/PM/input-en-US'))

WebUI.click(findTestObject('Object Repository/PM,TM,Translator flow/PM/option-FRODO_BAGGINS'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/PM,TM,Translator flow/PM/button_Assign managers'))

WebUI.delay(3)

WebUI.takeScreenshot()

