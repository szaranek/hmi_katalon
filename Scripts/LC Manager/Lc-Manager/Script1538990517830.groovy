import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.After

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/LC Manager/button_LC Settings'))

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/LC Manager/button_Upload LC file'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.uploadFile(findTestObject('Object Repository/LC Manager/lc-manager-upload-lengthCalculator-input'), GlobalVariable.LCPath)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/LC Manager/lc-manager-upload-lengthCalculator-button-upload'))

WebUI.delay(6)

WebUI.takeScreenshot()

if (WebUI.verifyTextPresent('File with this name already exists.', false, FailureHandling.OPTIONAL)) {
	
	WebUI.click(findTestObject('Object Repository/LC Manager/button_close (1)'))
		
}

else
{
	
	WebUI.click(findTestObject('Object Repository/LC Manager/lc-manager-upload-lengthCalculator-button-close'))

}

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/LC Manager/button_close'))

WebUI.delay(1)

WebUI.takeScreenshot()