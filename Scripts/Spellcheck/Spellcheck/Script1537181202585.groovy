import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Spellcheck/mt-translation-textarea-de-DE'), 'test')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Spellcheck/mt-translation-textarea-de-DE'))

WebUI.delay(2)

WebUI.focus(findTestObject('Spellcheck/mt-translation-textarea-de-DE'))

WebUI.delay(1)

WebUI.rightClick(findTestObject('Spellcheck/mt-translation-textarea-de-DE'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('Spellcheck/button_Add to dictionary'), 5)

WebUI.mouseOver(findTestObject('Spellcheck/button_Test'))

WebUI.click(findTestObject('Spellcheck/button_Test'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Spellcheck/mt-translation-textarea-de-DE'), 'Mutter')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.rightClick(findTestObject('Spellcheck/mt-translation-textarea-de-DE'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementNotPresent(findTestObject('Spellcheck/button_Add to dictionary'), 5)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.clearText(findTestObject('Spellcheck/mt-translation-textarea-de-DE'))

WebUI.refresh()

WebUI.delay(5)

WebUI.takeScreenshot()

