import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import tools.RandomString as RandomString

WebUI.clearText(findTestObject('Object Repository/MT/Edit Project/Project Properties/cnp-properties-input-projectName'))

WebUI.delay(1)

WebUI.takeScreenshot()

GlobalVariable.TRprojectname = ('editedkattest' + RandomString.randomString())

WebUI.setText(findTestObject('Object Repository/MT/Edit Project/Project Properties/cnp-properties-input-projectName'), GlobalVariable.TRprojectname)

GlobalVariable.TRproject = WebUI.modifyObjectProperty(GlobalVariable.TRproject, 'tid', 'starts with', 'dashboard-list-button-' + 
    GlobalVariable.TRprojectname, true)

GlobalVariable.ProjectMenu = WebUI.modifyObjectProperty(GlobalVariable.ProjectMenu, 'tid', 'starts with', 'dashboard-list-button-menu-' + 
    GlobalVariable.TRprojectname, true)

GlobalVariable.ExpandProjectArrow = WebUI.modifyObjectProperty(GlobalVariable.ExpandProjectArrow, 'id', 'starts with', 
    'dashboard-list-expand-' + GlobalVariable.TRprojectname, true)

GlobalVariable.NewCreatedTR = WebUI.modifyObjectProperty(GlobalVariable.NewCreatedTR, 'tid', 'contains', 'tr' + GlobalVariable.TRprojectname, 
    true)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.clearText(findTestObject('MT/Edit Project/Project Properties/cnp-properties-textarea-description'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Object Repository/MT/Edit Project/Project Properties/cnp-properties-textarea-description'), 
    'Edited description')

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('Object Repository/MT/Edit Project/Project Properties/cnp-properties-button-next'), 
    3)

WebUI.click(findTestObject('Object Repository/MT/Edit Project/Project Properties/cnp-properties-button-next'))

WebUI.delay(1)

WebUI.takeScreenshot()

