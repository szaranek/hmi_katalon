import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.callTestCase(findTestCase('LC Manager/Lc-Manager'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/div_Create ProjectTest'))

WebUI.takeScreenshot()

WebUI.delay(2)

//CustomKeywords.'tools.uploadFiles.uploadFile'(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/label_custom-file'), 'E:\\ProjectTestSupplier.xml')
WebUI.uploadFile(findTestObject('Object Repository/CNP/00 HMIT_1056_Upload_XML_File/label_custom-file'), GlobalVariable.XMLPath)

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.waitForElementVisible(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/b_File parsing completed'), 3)

WebUI.waitForElementVisible(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/b_Upload completed'), 3)

WebUI.delay(3)

WebUI.waitForElementVisible(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/Next'), 5)

WebUI.waitForElementClickable(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/Next'), 5)

WebUI.scrollToElement(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/Next'), 0)

WebUI.click(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/Next'))

WebUI.delay(1)

WebUI.takeScreenshot()

