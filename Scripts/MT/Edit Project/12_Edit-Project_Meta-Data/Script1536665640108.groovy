import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('CNP/04 HMIT_1052_Selecting_meta_data_method/span_Add New_Font_Bitmap'))

Thread.sleep(1000)

WebUI.takeScreenshot()

WebUI.uploadFile(findTestObject('CNP/04 HMIT_1052_Selecting_meta_data_method/label_upload_font'), GlobalVariable.bitmapFont)

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/04 HMIT_1052_Selecting_meta_data_method/button_Upload'))

WebUI.delay(5)

if(WebUI.verifyTextPresent('Error while uploading file', false, FailureHandling.OPTIONAL))
{

	WebUI.delay(1)
	
	WebUI.takeScreenshot()
	
	WebUI.click(findTestObject('Object Repository/CNP/04 HMIT_1052_Selecting_meta_data_method/patch 01.10.2018/button_close'))

	WebUI.delay(1)
	
	WebUI.takeScreenshot()
	
	WebUI.click(findTestObject('Object Repository/CNP/04 HMIT_1052_Selecting_meta_data_method/patch 01.10.2018/cnp-metaData-fonts-button-select-Bitmap'))
	
	WebUI.delay(1)
	
	WebUI.takeScreenshot()
	
	WebUI.click(findTestObject('Object Repository/CNP/04 HMIT_1052_Selecting_meta_data_method/patch 01.10.2018/mat-option_bitmapFont.zip'))

	}
else
{
	WebUI.click(findTestObject('CNP/04 HMIT_1052_Selecting_meta_data_method/button_Close'), FailureHandling.OPTIONAL)
}

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/04 HMIT_1052_Selecting_meta_data_method/span_Select_Vendor'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/04 HMIT_1052_Selecting_meta_data_method/div_exampleplugin1232334543554'))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('MT/Edit Project/Meta Data/cnp-metaData-button-next'), 3)

WebUI.delay(1)

WebUI.mouseOver(findTestObject('MT/Edit Project/Meta Data/cnp-metaData-button-next'))

WebUI.delay(1)

WebUI.doubleClick(findTestObject('MT/Edit Project/Meta Data/cnp-metaData-button-next'))

WebUI.delay(1)

WebUI.click(findTestObject('MT/Edit Project/Meta Data/cnp-metaData-button-next'),FailureHandling.OPTIONAL)

WebUI.takeScreenshot()

