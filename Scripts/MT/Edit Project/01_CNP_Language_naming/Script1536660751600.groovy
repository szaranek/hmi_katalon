import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.delay(GlobalVariable.WaitShort)


WebUI.setText(findTestObject('CNP/01 HMIT_1055_Language_naming/input_mat-input-2'), 'de-DE: German - Germany')

WebUI.click(findTestObject('CNP/01 HMIT_1055_Language_naming/span_de-DE German - Germany'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('CNP/01 HMIT_1055_Language_naming/input_mat-input-3'), 'de-AT: German - Austria')

WebUI.click(findTestObject('CNP/01 HMIT_1055_Language_naming/span_de-AT German - Austria'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('CNP/01 HMIT_1055_Language_naming/input_mat-input-4'), 'af-ZA: Afrikaans - South Africa')

WebUI.click(findTestObject('CNP/01 HMIT_1055_Language_naming/span_en-GB English - United Ki'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('CNP/01 HMIT_1055_Language_naming/input_mat-input-5'), 'ar-AE: Arabic - United Arab Emirates')

WebUI.click(findTestObject('CNP/01 HMIT_1055_Language_naming/span_en-US English - United St'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.waitForElementClickable(findTestObject('CNP/01 HMIT_1055_Language_naming/Next'), 10)

WebUI.click(findTestObject('CNP/01 HMIT_1055_Language_naming/Next'))

WebUI.delay(2)

WebUI.takeScreenshot()

