import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(8)

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/div_mat-select-arrow-wrapper'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/span_Editor'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/Left_FOO'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/i_fa fa-long-arrow-right fa-2x'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('Object Repository/MT/Edit Project/Invite Users/cnp-summary-button-create'), 3)

WebUI.click(findTestObject('Object Repository/MT/Edit Project/Invite Users/cnp-summary-button-create'))

WebUI.delay(1)

WebUI.takeScreenshot()

