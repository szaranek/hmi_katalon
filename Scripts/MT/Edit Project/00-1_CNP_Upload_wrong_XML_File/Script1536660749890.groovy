import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('LogIn/00_STC_Log_in_to_HMI_tool'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/div_Create ProjectTest'))

Thread.sleep(2000 //2500, 3000, other might work for you
    )

CustomKeywords.'tools.uploadFiles.uploadFile'(findTestObject('Object Repository/label_custom-file'), 'E:\\ProjectTestSupplier.xml')

Thread.sleep(2000 //2500, 3000, other might work for you
    )

WebUI.waitForElementVisible(findTestObject('CNP/00-1 HMIT_1029_Upload_wrong_XML_File/b_File parsing failed'), 3)

WebUI.waitForElementVisible(findTestObject('CNP/00-1 HMIT_1029_Upload_wrong_XML_File/b_Error occurred while file pa'), 3)

WebUI.takeScreenshot()

