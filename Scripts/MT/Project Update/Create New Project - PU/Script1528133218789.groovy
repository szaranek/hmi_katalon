import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import tools.RandomString as RandomString

WebUI.delay(2)

WebUI.click(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/div_Create ProjectTest'))

Thread.sleep(2000 //2500, 3000, other might work for you
    )

//CustomKeywords.'tools.uploadFiles.uploadFile'(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/label_custom-file'), 'E:\\ProjectTestSupplier.xml')
WebUI.uploadFile(findTestObject('Object Repository/CNP/00 HMIT_1056_Upload_XML_File/label_custom-file'), GlobalVariable.XMLPU)

Thread.sleep(2000 //2500, 3000, other might work for you
    )

WebUI.waitForElementVisible(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/b_File parsing completed'), 3)

WebUI.waitForElementVisible(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/b_Upload completed'), 3)

WebUI.takeScreenshot()

//WebUI.takeScreenshot()
WebUI.waitForElementVisible(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/Next'), 5)

WebUI.waitForElementClickable(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/Next'), 5)

WebUI.scrollToElement(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/Next'), 3)

WebUI.click(findTestObject('CNP/00 HMIT_1056_Upload_XML_File/Next'))

WebUI.takeScreenshot()

WebUI.delay(GlobalVariable.WaitShort)

//WebUI.click(findTestObject('CNP/01 HMIT_1055_Language_naming/input_mat-input-2'))
//WebUI.sendKeys(findTestObject(null), "de-DE")
WebUI.setText(findTestObject('CNP/01 HMIT_1055_Language_naming/input_mat-input-2'), 'de-DE: German - Germany')

WebUI.click(findTestObject('CNP/01 HMIT_1055_Language_naming/span_de-DE German - Germany'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('CNP/01 HMIT_1055_Language_naming/input_mat-input-3'), 'de-AT: German - Austria')

//WebUI.click(findTestObject('CNP/01 HMIT_1055_Language_naming/input_mat-input-3'))
//WebUI.sendKeys(findTestObject(null), "de-AT")
//
WebUI.click(findTestObject('CNP/01 HMIT_1055_Language_naming/span_de-AT German - Austria'), FailureHandling.STOP_ON_FAILURE)

WebUI.takeScreenshot()

//WebUI.takeScreenshot()
WebUI.waitForElementClickable(findTestObject('CNP/01 HMIT_1055_Language_naming/Next'), 10)

WebUI.scrollToElement(findTestObject('CNP/01 HMIT_1055_Language_naming/Next'), 3)

WebUI.click(findTestObject('CNP/01 HMIT_1055_Language_naming/Next'))

WebUI.click(findTestObject('CNP/02 HMIT_1054_Selecting_languages/input_mat-input-6'), FailureHandling.STOP_ON_FAILURE)

//WebUI.verifyElementText(findTestObject('CNP/02 HMIT_1054_Selecting_languages/span_de-DE_select'), 'de-DE')
WebUI.click(findTestObject('CNP/02 HMIT_1054_Selecting_languages/span_de-DE_select'), FailureHandling.STOP_ON_FAILURE)

//WebUI.takeScreenshot()
WebUI.click(findTestObject('CNP/02 HMIT_1054_Selecting_languages/Next'))

WebUI.delay(GlobalVariable.WaitShort)

GlobalVariable.ProjectName = ('kattestpu' + RandomString.randomString())

WebUI.setText(findTestObject('CNP/03 HMIT_1057_Create_project/input_inputProjectName'), GlobalVariable.ProjectName)

GlobalVariable.Project = WebUI.modifyObjectProperty(GlobalVariable.Project, 'tid', 'starts with', 'dashboard-list-button-' + GlobalVariable.ProjectName, 
    true)

//WebUI.click(findTestObject('CNP/03 HMIT_1057_Create_project/div_mat-select-arrow_brand'))
//WebUI.waitForElementVisible(findTestObject('CNP/03 HMIT_1057_Create_project/span_VW'), GlobalVariable.WaitShort)
//WebUI.click(findTestObject('CNP/03 HMIT_1057_Create_project/span_VW'))
WebUI.click(findTestObject('CNP/03 HMIT_1057_Create_project/div_mat-select-arrow_project'), FailureHandling.STOP_ON_FAILURE)

//WebUI.waitForElementClickable(findTestObject('CNP/03 HMIT_1057_Create_project/span_Generic'), GlobalVariable.WaitShort)
WebUI.click(findTestObject('CNP/03 HMIT_1057_Create_project/span_Generic'))

WebUI.takeScreenshot()

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('CNP/03 HMIT_1057_Create_project/Next'), 0)

//WebUI.takeScreenshot()
//WebUI.waitForElementClickable(findTestObject('CNP/03 HMIT_1057_Create_project/Next'), GlobalVariable.WaitMid)
WebUI.click(findTestObject('CNP/03 HMIT_1057_Create_project/Next'))

WebUI.delay(15)

WebUI.scrollToElement(findTestObject('CNP/04 HMIT_1052_Selecting_meta_data_method/Next'), 5)

WebUI.click(findTestObject('CNP/04 HMIT_1052_Selecting_meta_data_method/Next'))

WebUI.delay(8)

WebUI.scrollToElement(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/Next'), 5)

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/Next'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/span_Complete project'))

WebUI.delay(5)

