import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update/00_button_Project'))

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update/01_button_Update'))

WebUI.delay(1)

WebUI.uploadFile(findTestObject('MT/Project Update/Project Update/02_input_importFile'), GlobalVariable.XMLPath)

WebUI.delay(4)

WebUI.click(findTestObject('MT/Project Update/Project Update/03_button_Next'))

WebUI.delay(1)

WebUI.click(GlobalVariable.Project)

WebUI.delay(1)

WebUI.verifyElementClickable(findTestObject('MT/Project Update/Project Update/05-0_button_Previous'))

WebUI.click(findTestObject('MT/Project Update/Project Update/05-1_Next'))

WebUI.delay(1)

//
WebUI.waitForElementClickable(findTestObject('MT/Project Update/Project Update 2/01_next'), 5)

WebUI.verifyElementClickable(findTestObject('MT/Project Update/Project Update 2/00_Previous'))

WebUI.click(findTestObject('MT/Project Update/Project Update 2/01_next'))

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/02_lc-Addnew'))

WebUI.delay(1)

WebUI.uploadFile(findTestObject('MT/Project Update/Project Update 2/04_uploadfile'), GlobalVariable.LCPath)

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/02-1 uploadbutton'))

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/02-2 button_Close'))

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/03_lcv-select'))

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/03-1_exampleplugin.jar'))

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/03-2 Apply'))

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/05_fontvector-Addnew'))

WebUI.delay(1)

WebUI.uploadFile(findTestObject('MT/Project Update/Project Update 2/04_uploadfile'), GlobalVariable.FontPath)

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/02-1 uploadbutton'))

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/02-2 button_Close'))

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/06_fontbitmap-Addnew'))

WebUI.delay(1)

WebUI.uploadFile(findTestObject('MT/Project Update/Project Update 2/04_uploadfile'), GlobalVariable.FontPath)

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/02-1 uploadbutton'))

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/02-2 button_Close'))

WebUI.delay(2)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/07 Next'))

WebUI.delay(2)

WebUI.setText(findTestObject('MT/Project Update/Project Update 2/09 en-gb'), 'en-gb')

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/09-1_en-GB English - United Ki'))

WebUI.delay(1)

WebUI.setText(findTestObject('MT/Project Update/Project Update 2/10 en-us') ,'en-us')

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/10-1_en-US English - United St'))

WebUI.delay(1)

WebUI.verifyElementClickable(findTestObject('MT/Project Update/Project Update 2/12 Previous'))

WebUI.delay(1)

WebUI.click(findTestObject('MT/Project Update/Project Update 2/11 Next'))

WebUI.delay(1)
//
WebUI.click(findTestObject('MT/Project Update/Project Update/06_button_Import'))

WebUI.delay(2)

WebUI.verifyElementClickable(findTestObject('MT/Project Update/Project Update/08-0_button_Reject project'))

WebUI.click(findTestObject('MT/Project Update/Project Update/08-1_button_Save project'))

WebUI.delay(5)

WebUI.verifyElementNotVisible(findTestObject('MT/Project Update/Project Update/08-1_button_Save project'))


