import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Object Repository/MT/Project Update/Project Update 2/09-1_en-GB English - United Ki'), 'en-gb')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Project Update/Project Update 31.08/en-GB English - United Ki'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Object Repository/MT/Project Update/Project Update 2/10-1_en-US English - United St'), 'en-us')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Project Update/Project Update 31.08/en-US English - United St'))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('MT/Project Update/Project Update/03_button_Next'), 3)

WebUI.doubleClick(findTestObject('MT/Project Update/Project Update/03_button_Next'))

WebUI.delay(2)

WebUI.takeScreenshot()