import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update 04-10-2018/projectUpdate-comparison-checkbox-deleteOldInternalLabels'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update 04-10-2018/projectUpdate-comparison-checkbox-assignLabelsToUpdatesNodes'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update 04-10-2018/projectUpdate-comparison-checkbox-changeStatusOnFontAssignmentChange'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update 04-10-2018/projectUpdate-comparison-checkbox-changeStatusOnLineBreakModeChange'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update 04-10-2018/projectUpdate-comparison-checkbox-changeStatusOnMaximumValueIncrease'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update 04-10-2018/projectUpdate-comparison-checkbox-changeStatusOnMetadataChange'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update 04-10-2018/projectUpdate-comparison-checkbox-changeStatusOnPunctuationChange'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update 04-10-2018/projectUpdate-comparison-checkbox-changeStatusOnSourceChangeAboveThreshold'))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('MT/Project Update/Project Update/03_button_Next'), 3)

WebUI.doubleClick(findTestObject('MT/Project Update/Project Update/03_button_Next'))

WebUI.delay(2)

WebUI.takeScreenshot()