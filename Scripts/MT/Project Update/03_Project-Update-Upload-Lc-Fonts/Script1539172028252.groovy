import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


WebUI.delay(1)
	
WebUI.takeScreenshot()
	
WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update patch 02.10.2018/cnp-updateProject-metaData-lengthCalculator-button-selectVendor-MyLengthCalculation'))

WebUI.delay(1)
	
WebUI.takeScreenshot()
	
WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update patch 02.10.2018/cnp-updateProject-metaData-lengthCalculation-select-exampleplugin.jar'))
	
WebUI.delay(1)
	
WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update 04-10-2018/cnp-updateProject-metaData-button-updateLengthCalculationFile'))

WebUI.delay(1)
	
WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update patch 02.10.2018/cnp-updateProject-metaData-lengthCalculation-select-exampleplugin.jar'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/04 HMIT_1052_Selecting_meta_data_method/button_Close'), FailureHandling.OPTIONAL)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Project Update/Project Update 2/03_lcv-select'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Project Update/Project Update 2/03-1_exampleplugin.jar'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Project Update/Project Update 2/03-2 Apply'))

WebUI.delay(1)

WebUI.takeScreenshot()
	
WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update patch 02.10.2018/cnp-updateProject-metaData-fonts-button-select-Vector'))
	
WebUI.delay(1)
	
WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update patch 02.10.2018/cnp-updateProject-metaData-fonts-select-FontTest.zip'))
	
WebUI.delay(1)
	
WebUI.takeScreenshot()
	
WebUI.scrollToElement(findTestObject('Object Repository/MT/Project Update/Project Update 04-10-2018/cnp-updateProject-metaData-button-updateProjectFontFile'), 3)
	
WebUI.delay(1)
	
WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Project Update/Project Update 04-10-2018/cnp-updateProject-metaData-button-updateProjectFontFile'))
	
WebUI.delay(1)
	
WebUI.takeScreenshot()

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('MT/Project Update/Project Update/03_button_Next'), 3)

WebUI.doubleClick(findTestObject('MT/Project Update/Project Update/03_button_Next'))
