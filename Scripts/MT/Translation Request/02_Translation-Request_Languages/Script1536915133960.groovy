import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import tools.RandomString

WebUI.click(findTestObject('MT/Translation Request/Translation Request/02_mt-languages-button-addNewSourceLanguage'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Translation Request/Translation Request/03_button_remove Remove last sour'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Translation Request/Translation Request/04_mt-languages-button-editFilters'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Translation Request/Translation Request/05_mt-languages-editFilters-button-addFilter'))

WebUI.delay(1)

WebUI.takeScreenshot()

GlobalVariable.TRFilterName = ('KAT_FILTER_' + RandomString.randomString())

WebUI.delay(1)

WebUI.setText(findTestObject('MT/Translation Request/Translation Request/06_mt-languages-editFilters-input-filterName'), GlobalVariable.TRFilterName)

WebUI.delay(1)

WebUI.takeScreenshot()

GlobalVariable.TRFilter = WebUI.modifyObjectProperty(GlobalVariable.TRFilter, 'text', 'equals', GlobalVariable.TRFilterName,
	true)

WebUI.delay(1)


GlobalVariable.TRFilterSelect = WebUI.modifyObjectProperty(GlobalVariable.TRFilterSelect, 'tid', 'equals', 'mt-languages-input-filter-' +
	GlobalVariable.TRFilterName, true)

WebUI.delay(1)

WebUI.setText(findTestObject('MT/Translation Request/Translation Request/08_mt-languages-editFilters-inputSearchQuery'), 'sourceText~"Hello"')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('MT/Translation Request/Translation Request/09_mt-translationRequest-button-saveAndGoToFilter'),
	3)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Translation Request/Translation Request/09_mt-translationRequest-button-saveAndGoToFilter'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementVisible(GlobalVariable.TRFilter)

WebUI.click(findTestObject('MT/Translation Request/Translation Request/11_mt-translationRequest-button-close'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Translation Request/Translation Request/mt-languages-input-translationLanguages'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Translation Request/Translation Request/mat-option_Check all'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.clickOffset(findTestObject('MT/Translation Request/Translation Request/01_mt-translationRequest-button-next'),0,0)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Translation Request/Translation Request/mt-languages-input-filter'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(GlobalVariable.TRFilterSelect)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('MT/Translation Request/Translation Request/01_mt-translationRequest-button-next'), 4)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Translation Request/Translation Request/01_mt-translationRequest-button-next'))

WebUI.delay(1)

WebUI.takeScreenshot()