import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import tools.RandomString as RandomString

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/MT/Report/00_Project_button'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/MT/Report/01_Report_button'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/MT/Report/02_Precheck_button'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/MT/Report/03_button_next'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/MT/Report/04_Edit_button'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/MT/Report/05_Add_button'))

WebUI.delay(2)

GlobalVariable.ReFilterName = ('KAT_FILTER_' + RandomString.randomString())

WebUI.setText(findTestObject('Object Repository/MT/Report/06_Filter_name_input'), GlobalVariable.ReFilterName)

GlobalVariable.ReFilter = WebUI.modifyObjectProperty(GlobalVariable.ReFilter, 'tid', 'starts with','mt-projectReporting-filtering-select-' + GlobalVariable.ReFilterName, 
    true)

WebUI.setText(findTestObject('Object Repository/MT/Report/07_Advanced_search_input'), 'sourceText~"Hello"')

WebUI.delay(1)

WebUI.verifyElementClickable(findTestObject('Object Repository/MT/Report/08_Filtering_button'))

WebUI.click(findTestObject('Object Repository/MT/Report/09_Filter_button'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/MT/Report/10_Close_button'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/MT/Report/11_Filter_input'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(GlobalVariable.ReFilter)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.clickOffset(findTestObject('Object Repository/MT/Report/14_button_next'), 0, 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/MT/Report/13_button_previous'))

WebUI.click(findTestObject('Object Repository/MT/Report/14_button_next'))

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.verifyElementClickable(findTestObject('Object Repository/MT/Report/15_button_previous'))

WebUI.click(findTestObject('Object Repository/MT/Report/16_button_next'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Report/17_button_run the export'))

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Report/18_button_preview (new tab)'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Report/20_close-button'))

WebUI.delay(1)

WebUI.takeScreenshot()