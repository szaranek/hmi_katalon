import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.clickOffset(findTestObject('MT/MT_Settings/settings_button'), 0, 0)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Tabs/div_Mapping settings'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/MT_Settings/Mapping_settings/mt-project-settings-penalties-section-minRatingScore-number-input'), 
    '35')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Mapping_settings/mt-project-settings-penalties-section-minRatingScore-button-up'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Mapping_settings/mt-project-settings-penalties-section-minRatingScore-button-down'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/MT_Settings/Mapping_settings/mt-project-settings-penalties-section-maxRecords-number-input'), 
    '35')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Mapping_settings/mt-project-settings-penalties-section-maxRecords-button-up'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Mapping_settings/mt-project-settings-penalties-section-maxRecords-button-down'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/MT_Settings/Mapping_settings/mt-project-settings-penalties-section-penaltyForTextType-number-input'), 
    '35')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Mapping_settings/mt-project-settings-penalties-section-penaltyForTextType-button-up'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Mapping_settings/mt-project-settings-penalties-section-penaltyForTextType-button-down'))

WebUI.delay(1)

WebUI.takeScreenshot()

//WebUI.click(findTestObject('MT/MT_Settings/Mapping_settings/mapping-settings-select-de-AT German - Austria'))
//
//WebUI.delay(1)
//
//WebUI.takeScreenshot()
//
//WebUI.click(findTestObject('MT/MT_Settings/Mapping_settings/mapping-settings-arrow-right'))
//
//WebUI.delay(1)
//
//WebUI.takeScreenshot()
//
//WebUI.click(findTestObject('MT/MT_Settings/Mapping_settings/div_de-AT German - Austria'))
//
//WebUI.delay(1)
//
//WebUI.takeScreenshot()
//
//WebUI.click(findTestObject('MT/MT_Settings/Mapping_settings/mapping-settings-arrow-left'))
//
//WebUI.delay(1)
//
//WebUI.takeScreenshot()
WebUI.click(findTestObject('MT/MT_Settings/Mapping_settings/mt-project-settings-mapping-section-button-submit'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.delay(5)

