import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.clickOffset(findTestObject('MT/MT_Settings/settings_button'), 0, 0)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Tabs/div_TM penalties'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/MT_Settings/TM_penalties/mt-project-settings-tm-penalties-input-score-number-input'), '35')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/TM_penalties/mt-project-settings-tm-penalties-input-score-button-up'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/TM_penalties/mt-project-settings-tm-penalties-input-score-button-down'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/MT_Settings/TM_penalties/mt-project-settings-tm-penalties-input-results-number-input'), 
    '35')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/TM_penalties/mt-project-settings-tm-penalties-input-results-button-up'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/TM_penalties/mt-project-settings-tm-penalties-input-results-button-down'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/MT_Settings/TM_penalties/mt-project-settings-tm-penalties-input-project-number-input'), 
    '35')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/TM_penalties/mt-project-settings-tm-penalties-input-project-number-input'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/TM_penalties/mt-project-settings-tm-penalties-input-project-button-down'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/TM_penalties/mt-project-settings-tm-penalties-onoff-toggle-score'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/TM_penalties/mt-project-settings-tm-penalties-onoff-toggle-score'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/TM_penalties/mt-project-settings-penalties-section-button-submit'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.delay(5)

