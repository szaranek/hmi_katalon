import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.clickOffset(findTestObject('MT/MT_Settings/settings_button'), 0, 0)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/mat-tab-header-pagination mat-tab-header-pagination-after mat-elevation-z4 mat-ripple'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/MT/MT_Settings/Columns_configuration/div_Nodes table settings'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Columns_configuration/project-settings-column-configuration-add-new-set-button'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/MT_Settings/Columns_configuration/project-settings-column-configuration-set-undefined-name-input'), 
    'ColumnSetKatalon')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Columns_configuration/project-settings-column-configuration-the-column-alignment'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Columns_configuration/project-settings-column-configuration-select-column-button'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Columns_configuration/project-settings-column-configuration-the-column-lines'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Columns_configuration/project-settings-column-configuration-select-column-button'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Columns_configuration/project-settings-column-configuration-set-undefined-save'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Columns_configuration/project-settings-column-configuration-set-1-edit'))

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Columns_configuration/project-settings-column-configuration-set-1-edit'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/MT_Settings/Columns_configuration/project-settings-column-configuration-set-undefined-name-input'), 
    'ColumnSetKatalonEdit')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Columns_configuration/project-settings-column-configuration-set-undefined-save'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Columns_configuration/project-settings-column-configuration-set-1-asActive'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/MT_Settings/Columns_configuration/project-settings-column-configuration-set-undefined-delete'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.delay(1)

WebUI.click(findTestObject('MT/MT_Settings/Columns_configuration/button_Yes'))

