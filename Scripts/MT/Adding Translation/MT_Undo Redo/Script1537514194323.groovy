import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('MT/MT_Tree/textarea_same-font ng-untouche'), 2)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/MT_Tree/textarea_same-font ng-untouche'), 'Test1')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyTextPresent('Test1', false, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('MT/MT_Tree/textarea_same-font ng-untouche'), 'Test2')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyTextPresent('Test2', false, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.CONTROL, 'z'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyTextPresent('Test1', false, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.CONTROL, 'y'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyTextPresent('Test2', false, FailureHandling.STOP_ON_FAILURE)
