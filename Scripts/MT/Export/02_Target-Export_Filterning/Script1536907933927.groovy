import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import tools.RandomString as RandomString

WebUI.click(findTestObject('MT/Export/02_mt-targetExport-filtering-button-editFilters'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Export/03_mt-targetExport-filtering-button-addNextFilter'))

WebUI.delay(1)

WebUI.takeScreenshot()

GlobalVariable.ExFilterName = ('KAT_FILTER_' + RandomString.randomString())

WebUI.setText(findTestObject('MT/Export/04-0_mt-targetExport-addNewFilter-input-name'), GlobalVariable.ExFilterName)

GlobalVariable.ExFilter = WebUI.modifyObjectProperty(GlobalVariable.ExFilter, 'tid', 'starts with', 'mt-targetExport-filtering-select-' + 
    GlobalVariable.ExFilterName, true)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('MT/Export/05_mt-targetExport-addNewFilter-inputSearchQuery'), 'sourceText~"Hello"')

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.verifyElementClickable(findTestObject('MT/Export/06_mt-targetExport-footer-button-saveGoToFiltering'))

WebUI.click(findTestObject('MT/Export/07_mt-targetExport-footer-button-saveGoToFilteringList'))

WebUI.delay(1)

WebUI.takeScreenshot()

//WebUI.verifyTextPresent(GlobalVariable.ExFilterName, false)

WebUI.click(findTestObject('MT/Export/08_mt-targetExport-footer-button-close'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('MT/Export/09_mt-targetExport-filtering-select'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(GlobalVariable.ExFilter)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.clickOffset(findTestObject('MT/Export/01_mt-targetExport-footer-button-next'), 0, 0, FailureHandling.OPTIONAL)

//WebUI.delay(1)
//
//WebUI.takeScreenshot()
//
//WebUI.click(findTestObject('MT/Export/01_mt-targetExport-footer-button-next'))

WebUI.delay(1)

WebUI.takeScreenshot()

