import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword as WebUIAbstractKeyword
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Object Repository/MT/Navigation/mt-treeGrid-row-enterNewChangeReEnterNewContactAgainNewContactEntryInputAgain'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Object Repository/MT/Grammar Parser/mt-translation-textarea-de-DE'), 'enter [new] | change | re-enter | new) contact [again] | [new] (contact) (entry | input) [again]')

WebUI.delay(4)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Grammar Parser/Save Green'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyTextPresent('Error', false)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Grammar Parser/button_Close'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.clearText(findTestObject('Object Repository/MT/Grammar Parser/mt-translation-textarea-de-DE'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Object Repository/MT/Grammar Parser/mt-translation-textarea-de-DE'), '(enter [new] | change | re-enter | new) contact [again] | [new] (contact) (entry | input) [again]')

WebUI.delay(4)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Grammar Parser/Save Green'))

WebUI.delay(1)

WebUI.takeScreenshot()

