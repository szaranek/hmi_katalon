import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.refresh()

WebUI.delay(10)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/mt-treeGrid-row-tuner-'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToPosition(0, 0)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.sendKeys(null, Keys.chord(Keys.ARROW_RIGHT))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/mt-treeGrid-row-tunerErrorPopup-'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.sendKeys(null, Keys.chord(Keys.ARROW_RIGHT))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/mt-treeGrid-row-langMyConcat-'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.sendKeys(null, Keys.chord(Keys.ARROW_RIGHT))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/mt-treeGrid-row-currentSpeed1CurrentDirection2-'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/properties-tab'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('Object Repository/MT/Placeholders/td_1 2'), 3)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('Object Repository/MT/Placeholders/td_1 2'), 3)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/mt-properties-span-edit'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/mt-placeholdeerSettingModal-input-selectLanguage'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/mt-placeholdeerSettingModal-inputOption-selectLanguage-German - Germany'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/mt-placeholdeerSettingModal-select-selectType-0'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/mt-placeholdeerSettingModal-selectOption-selectType-Number-0'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Object Repository/MT/Placeholders/mt-placeholdeerSettingModal-input-maxNumberValue0'), '5')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/mt-placeholdeerSettingModal-select-selectType-1'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/mt-placeholdeerSettingModal-selectOption-selectType-FixText-1'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Object Repository/MT/Placeholders/mt-placeholdeerSettingModal-input-fixText-1'), 'Test')

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/mt-placeholdeerSettingModal-checkbox-extraRow-0'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/MT/Placeholders/mt-placeholdeerSettingModal-button-Save'))

WebUI.delay(1)

WebUI.takeScreenshot()