import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.refresh()

WebUI.delay(8)

WebUI.scrollToPosition(0, 0)

//WebUI.takeScreenshot()
//
//WebUI.click(findTestObject('Object Repository/Translation Memory/mt-treeGrid-row-helloMoon'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Translation Memory/tm-tab'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Object Repository/Translation Memory/tm-search-input-serachTranslationMemory'), 'Hello world')

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('Object Repository/Translation Memory/tm-search-cell-selectRow-dblclick0'), 2)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.doubleClick(findTestObject('Object Repository/Translation Memory/tm-search-cell-selectRow-dblclick0'))

WebUI.delay(4)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Translation Memory/mt-standardText-button-saveAsCompleted'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.verifyElementNotPresent(findTestObject('Object Repository/Translation Memory/EMPTY_TRANSLATION_CHECK'), 5)

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Translation Memory/shared-modalError-button-save'))

WebUI.delay(2)

WebUI.takeScreenshot()