import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import tools.RandomString as RandomString

WebUI.delay(GlobalVariable.WaitShort)

GlobalVariable.TRprojectname = ('kattestsuperhmi' + RandomString.randomString())

WebUI.setText(findTestObject('CNP/03 HMIT_1057_Create_project/input_inputProjectName'), GlobalVariable.TRprojectname)

GlobalVariable.TRproject = WebUI.modifyObjectProperty(GlobalVariable.TRproject, 'tid', 'starts with', 'dashboard-list-button-' +
	GlobalVariable.TRprojectname, true)

GlobalVariable.ProjectMenu = WebUI.modifyObjectProperty(GlobalVariable.ProjectMenu, 'tid', 'contains', 'menu-' + GlobalVariable.TRprojectname, true)

WebUI.click(findTestObject('CNP/03 HMIT_1057_Create_project/div_mat-select-arrow_project'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('CNP/03 HMIT_1057_Create_project/span_Generic'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/03 HMIT_1057_Create_project/Next'))

