import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.delay(2)

WebUI.waitForElementClickable(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/div_mat-select-arrow-wrapper'), 
    10)

WebUI.waitForPageLoad(10)

WebUI.delay(5)

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/div_mat-select-arrow-wrapper'))

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/span_Editor'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/Left_FOO'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/i_fa fa-long-arrow-right fa-2x'))

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/Left_HARRY'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/i_fa fa-long-arrow-right fa-2x'))

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/RIGHT_FOO'))

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/Right_HARRY'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/i_fa fa-long-arrow-left fa-2x'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/Next'))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.click(findTestObject('CNP/06 HMIT_1050_Inviting_users_to_project/span_Complete project'))

WebUI.delay(2)

WebUI.takeScreenshot()

