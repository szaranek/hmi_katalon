
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import com.kms.katalon.core.testobject.TestObject

import java.lang.String


def static "tools.clickUsingJS.clickUsingJS"(
    	TestObject to	
     , 	int timeout	) {
    (new tools.clickUsingJS()).clickUsingJS(
        	to
         , 	timeout)
}

def static "tools.uploadFiles.uploadFile"(
    	TestObject to	
     , 	String filePath	) {
    (new tools.uploadFiles()).uploadFile(
        	to
         , 	filePath)
}
