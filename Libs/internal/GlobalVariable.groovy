package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object WaitShort
     
    /**
     * <p></p>
     */
    public static Object WaitMid
     
    /**
     * <p></p>
     */
    public static Object WaitLong
     
    /**
     * <p></p>
     */
    public static Object LCPath
     
    /**
     * <p></p>
     */
    public static Object XMLPath
     
    /**
     * <p></p>
     */
    public static Object FontPath
     
    /**
     * <p></p>
     */
    public static Object chars
     
    /**
     * <p></p>
     */
    public static Object length
     
    /**
     * <p></p>
     */
    public static Object GroupName
     
    /**
     * <p></p>
     */
    public static Object Group
     
    /**
     * <p></p>
     */
    public static Object testCaseFailed
     
    /**
     * <p></p>
     */
    public static Object URL
     
    /**
     * <p></p>
     */
    public static Object XMLPU
     
    /**
     * <p></p>
     */
    public static Object Project
     
    /**
     * <p></p>
     */
    public static Object ProjectName
     
    /**
     * <p></p>
     */
    public static Object ExFilterName
     
    /**
     * <p></p>
     */
    public static Object ExFilter
     
    /**
     * <p></p>
     */
    public static Object TRFilterName
     
    /**
     * <p></p>
     */
    public static Object TRFilter
     
    /**
     * <p></p>
     */
    public static Object TRFilterSelect
     
    /**
     * <p></p>
     */
    public static Object ReFilterName
     
    /**
     * <p></p>
     */
    public static Object ReFilter
     
    /**
     * <p></p>
     */
    public static Object SubGroupName
     
    /**
     * <p></p>
     */
    public static Object SubGroup
     
    /**
     * <p></p>
     */
    public static Object TranslationName
     
    /**
     * <p></p>
     */
    public static Object Translation
     
    /**
     * <p></p>
     */
    public static Object EditedGroupName
     
    /**
     * <p></p>
     */
    public static Object EditedGroup
     
    /**
     * <p></p>
     */
    public static Object TRprojectname
     
    /**
     * <p></p>
     */
    public static Object TRproject
     
    /**
     * <p></p>
     */
    public static Object TestSuitesPassed
     
    /**
     * <p></p>
     */
    public static Object ProjectMenu
     
    /**
     * <p></p>
     */
    public static Object ExpandProjectArrow
     
    /**
     * <p></p>
     */
    public static Object NewCreatedTR
     
    /**
     * <p></p>
     */
    public static Object HelpSystemName
     
    /**
     * <p></p>
     */
    public static Object HSgroup
     
    /**
     * <p></p>
     */
    public static Object HSsubgroup
     
    /**
     * <p></p>
     */
    public static Object HSpage
     
    /**
     * <p></p>
     */
    public static Object HSaddsub
     
    /**
     * <p></p>
     */
    public static Object HSaddpage
     
    /**
     * <p></p>
     */
    public static Object TRSubProject
     
    /**
     * <p></p>
     */
    public static Object TRSubProjectName
     
    /**
     * <p></p>
     */
    public static Object bitmapFont
     
    /**
     * <p></p>
     */
    public static Object TRenus
     
    /**
     * <p></p>
     */
    public static Object TRdeat
     
    /**
     * <p></p>
     */
    public static Object TRengb
     

    static {
        def allVariables = [:]        
        allVariables.put('default', ['WaitShort' : '2', 'WaitMid' : '5', 'WaitLong' : '10', 'LCPath' : '/opt/Katalon_Studio/Files/exampleplugin.jar', 'XMLPath' : '/opt/Katalon_Studio/Files/ProjectTestSupplier.xml', 'FontPath' : '/opt/Katalon_Studio/Files/FontTest.zip', 'chars' : '0123456789', 'length' : 4, 'GroupName' : '', 'Group' : findTestObject('STC/HMIT-1049_Adding_main_group/STC_List_Root_Group_test'), 'testCaseFailed' : false, 'URL' : 'http://145.239.90.33/login', 'XMLPU' : '/opt/Katalon_Studio/Files/ProjectTestSupplierPU.xml', 'Project' : findTestObject('MT/Project Update/Project Update/04_KAT_TEST'), 'ProjectName' : '', 'ExFilterName' : '', 'ExFilter' : findTestObject('MT/Export/04-1_Created-Filter-Name'), 'TRFilterName' : '', 'TRFilter' : findTestObject('MT/Translation Request/Translation Request/filterNameTest'), 'TRFilterSelect' : findTestObject('MT/Translation Request/Translation Request/FilterSelect'), 'ReFilterName' : '', 'ReFilter' : findTestObject('MT/Report/12_Filter_name'), 'SubGroupName' : '', 'SubGroup' : findTestObject('STC/HMIT_1048_Adding_group_to_selected_group/STC_List_Add_Sub_Object'), 'TranslationName' : '', 'Translation' : findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/STC_TREE_Object'), 'EditedGroupName' : '', 'EditedGroup' : findTestObject('STC/HMIT_1046_Editing_group/STC_Group'), 'TRprojectname' : '', 'TRproject' : findTestObject('MT/Translation Request/Translation Request/00 CNP project'), 'TestSuitesPassed' : 0, 'ProjectMenu' : findTestObject('MT/Mapping Assistance/00_project_dots'), 'ExpandProjectArrow' : findTestObject('MT/Mapping Assistance/05-1_dashboard-list-expand-(projectName)'), 'NewCreatedTR' : findTestObject('MT/Mapping Assistance/08_newCreatedTR'), 'HelpSystemName' : '', 'HSgroup' : findTestObject('Help System/Add Root Group/helpSystem-toggleGroup-name'), 'HSsubgroup' : findTestObject('Help System/Adding Subgroup/helpSystem-toggleGroup-name'), 'HSpage' : findTestObject('Help System/Adding Page/helpSystem-openPage-'), 'HSaddsub' : findTestObject('Help System/Adding Subgroup/helpSystem-addSubgroup-name'), 'HSaddpage' : findTestObject('Help System/Adding Page/helpSystem-addPage-name'), 'TRSubProject' : findTestObject('PM,TM,Translator flow/PM/dashboard-list-button-name-subProjectMenu'), 'TRSubProjectName' : '', 'bitmapFont' : '/opt/Katalon_Studio/Files/bitmapFont.zip', 'TRenus' : findTestObject('PM,TM,Translator flow/Translator/TR-en-us'), 'TRdeat' : findTestObject('PM,TM,Translator flow/Translator/TR-de-at'), 'TRengb' : findTestObject('PM,TM,Translator flow/Translator/TR-en-gb')])
        allVariables.put('Windows', allVariables['default'] + ['WaitShort' : '2', 'WaitMid' : '5', 'WaitLong' : '10', 'LCPath' : 'C:\\Katalon Studio\\Files\\exampleplugin.jar', 'XMLPath' : 'C:\\Katalon Studio\\Files\\ProjectTestSupplier.xml', 'FontPath' : 'C:\\Katalon Studio\\Files\\FontTest.zip', 'chars' : '0123456789', 'length' : 4, 'GroupName' : '', 'Group' : findTestObject('STC/HMIT-1049_Adding_main_group/STC_List_Root_Group_test'), 'testCaseFailed' : false, 'URL' : 'http://145.239.90.33/login', 'XMLPU' : 'C:\\Katalon Studio\\Files\\ProjectTestSupplierPU.xml', 'Project' : findTestObject('MT/Project Update/Project Update/04_KAT_TEST'), 'ProjectName' : '', 'ExFilterName' : '', 'ExFilter' : findTestObject('MT/Export/04-1_Filter_Name'), 'TRFilterName' : '', 'TRFilter' : findTestObject('MT/Translation Request/Translation Request/10_filter_name'), 'TRFilterSelect' : findTestObject('MT/Translation Request/Translation Request/Filter_select'), 'ReFilterName' : '', 'ReFilter' : findTestObject('MT/Report/12_Filter_name'), 'SubGroupName' : '', 'SubGroup' : findTestObject('STC/HMIT_1048_Adding_group_to_selected_group/STC_List_Add_Sub_Object'), 'TranslationName' : '', 'Translation' : findTestObject('STC/HMIT_1047_Adding_text_to_selected_group/STC_TREE_Object'), 'EditedGroupName' : '', 'EditedGroup' : findTestObject('STC/HMIT_1046_Editing_group/STC_Group'), 'TRprojectname' : '', 'TRproject' : findTestObject('MT/Translation Request/Translation Request/00 CNP project'), 'TestSuitesPassed' : 0, 'ProjectMenu' : findTestObject('MT/Mapping Assistance/00_project_dots'), 'ExpandProjectArrow' : findTestObject('MT/Mapping Assistance/05-1_dashboard-list-expand-(projectName)'), 'NewCreatedTR' : findTestObject('MT/Mapping Assistance/08_newCreatedTR'), 'bitmapFont' : 'C:\\Katalon Studio\\Files\\bitmapFont.zip'])
        
        String profileName = RunConfiguration.getExecutionProfile()
        
        def selectedVariables = allVariables[profileName]
        WaitShort = selectedVariables['WaitShort']
        WaitMid = selectedVariables['WaitMid']
        WaitLong = selectedVariables['WaitLong']
        LCPath = selectedVariables['LCPath']
        XMLPath = selectedVariables['XMLPath']
        FontPath = selectedVariables['FontPath']
        chars = selectedVariables['chars']
        length = selectedVariables['length']
        GroupName = selectedVariables['GroupName']
        Group = selectedVariables['Group']
        testCaseFailed = selectedVariables['testCaseFailed']
        URL = selectedVariables['URL']
        XMLPU = selectedVariables['XMLPU']
        Project = selectedVariables['Project']
        ProjectName = selectedVariables['ProjectName']
        ExFilterName = selectedVariables['ExFilterName']
        ExFilter = selectedVariables['ExFilter']
        TRFilterName = selectedVariables['TRFilterName']
        TRFilter = selectedVariables['TRFilter']
        TRFilterSelect = selectedVariables['TRFilterSelect']
        ReFilterName = selectedVariables['ReFilterName']
        ReFilter = selectedVariables['ReFilter']
        SubGroupName = selectedVariables['SubGroupName']
        SubGroup = selectedVariables['SubGroup']
        TranslationName = selectedVariables['TranslationName']
        Translation = selectedVariables['Translation']
        EditedGroupName = selectedVariables['EditedGroupName']
        EditedGroup = selectedVariables['EditedGroup']
        TRprojectname = selectedVariables['TRprojectname']
        TRproject = selectedVariables['TRproject']
        TestSuitesPassed = selectedVariables['TestSuitesPassed']
        ProjectMenu = selectedVariables['ProjectMenu']
        ExpandProjectArrow = selectedVariables['ExpandProjectArrow']
        NewCreatedTR = selectedVariables['NewCreatedTR']
        HelpSystemName = selectedVariables['HelpSystemName']
        HSgroup = selectedVariables['HSgroup']
        HSsubgroup = selectedVariables['HSsubgroup']
        HSpage = selectedVariables['HSpage']
        HSaddsub = selectedVariables['HSaddsub']
        HSaddpage = selectedVariables['HSaddpage']
        TRSubProject = selectedVariables['TRSubProject']
        TRSubProjectName = selectedVariables['TRSubProjectName']
        bitmapFont = selectedVariables['bitmapFont']
        TRenus = selectedVariables['TRenus']
        TRdeat = selectedVariables['TRdeat']
        TRengb = selectedVariables['TRengb']
        
    }
}
