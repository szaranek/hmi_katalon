<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Create SuperHMI</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-10-22T12:33:27</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>aeb6b048-026a-4014-a7ae-2da7f3be11fa</testSuiteGuid>
   <testCaseLink>
      <guid>39f4623f-b2ea-40d5-9fe3-308ac75bd711</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_CNP_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed68cdba-e478-4b99-83d6-5088dd1bf78e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP - SuperHMI/00_CNP_SuperHMI_Upload_XML_File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>129b4dc8-62c7-4a4f-8cd7-ecab719aa056</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP - SuperHMI/01_CNP_SuperHMI_Language_naming</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3950357f-f1a4-4d9e-8cb0-df06fd82cb77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP - SuperHMI/02_CNP_SuperHMI_Selecting_languages</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34131680-dfda-4b3c-8a36-bcaf8e10950f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP - SuperHMI/03_CNP_SuperHMI_Properties</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38402b5a-c1ea-417b-b4f3-b6539a85ed63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP - SuperHMI/04_CNP_SuperHMI_Meta-Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4aa727c5-dacf-49dc-884a-3d29deb51009</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP - SuperHMI/05_CNP_SuperHMI_Selecting_alternative_fonts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a34236e-2232-41d0-aee5-6f91d3aa5980</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP - SuperHMI/06_CNP_SuperHMI_Inviting_users_to_project</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
