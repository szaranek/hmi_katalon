<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Grammar Parser, Target Export, MT-Search</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-07-29T16:40:06</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9e0bd2be-7e65-4c66-820f-d616d551335a</testSuiteGuid>
   <testCaseLink>
      <guid>d30822a4-d199-4756-bbf1-84a8a3dc15cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/HMI-VERSION/HMI_Version</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26a6814d-81f4-4c9c-b350-0a638c6e37fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_CNP_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96842bd0-a77a-48f0-a1e1-b3f08ff9af77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/00_CNP_Upload_XML_File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9bad835f-d291-4722-a892-b51571ba175c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/01_CNP_Language_naming</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a25965eb-f420-43a5-b30e-ca24aa160de6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/02_CNP_Selecting_languages</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>008066e8-71a2-4c37-a264-921825f2d37a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/03_CNP_Properties</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4a5ac0b-65c3-47c7-b4b2-dcf23ad674e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/04_CNP_Meta-Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ea7746a-f154-4fb8-8ffc-a4438d1e087c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/05_CNP_Selecting_alternative_fonts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83206e1d-5f93-4d50-8062-482afe549b5d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/06_CNP_Inviting_users_to_project</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>66c14a97-24a1-44a9-b347-94e26f214ac6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/00_MT_GoToMT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbf706e1-aa80-4c46-8f67-4409f3ce4e73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Navigation/Navigation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4314889-29a7-45b6-a333-d2123461e21d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Grammar Parser/Grammar-Parser_speech-prompt</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>206b4a88-71ca-42c2-aa09-7902ee192f98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Grammar Parser/Grammar-Parser_speech-command</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d284f5e1-c993-46a7-8480-0e781ae18066</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/FontTable/FontTable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0ffa97e-9df7-4d38-bd6f-2196415f02cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/09_MT_Search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bbf0196c-02d4-4d65-b408-7dc195be9a2c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Export/00_MoveToTargetExport</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d2567a8-19f9-4ad1-b7c2-1191f52ff74d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Export/01_Target-Export_Precheck</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dbaa38df-4fda-4334-bc1a-6e6bd99a1f44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Export/02_Target-Export_Filterning</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>974406c6-db00-469e-ab78-2ceb87fbbdec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Export/03_Target-Export_Summary</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>61ea7038-a24a-426f-b4cc-f7512cf1f944</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Export/04_Target-Export_Running-the-export</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
