<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Translation Request with PM and TM flow</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-10-22T13:56:20</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6fba50a3-4704-49dd-88b5-feb512e5eb07</testSuiteGuid>
   <testCaseLink>
      <guid>26a6814d-81f4-4c9c-b350-0a638c6e37fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_CNP_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96842bd0-a77a-48f0-a1e1-b3f08ff9af77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/00_CNP_Upload_XML_File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9bad835f-d291-4722-a892-b51571ba175c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/01_CNP_Language_naming</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a25965eb-f420-43a5-b30e-ca24aa160de6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/02_CNP_Selecting_languages</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>008066e8-71a2-4c37-a264-921825f2d37a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/03_CNP_Properties</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4a5ac0b-65c3-47c7-b4b2-dcf23ad674e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/04_CNP_Meta-Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ea7746a-f154-4fb8-8ffc-a4438d1e087c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/05_CNP_Selecting_alternative_fonts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83206e1d-5f93-4d50-8062-482afe549b5d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/06_CNP_Inviting_users_to_project</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>66c14a97-24a1-44a9-b347-94e26f214ac6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/00_MT_GoToMT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3fa84b9-6371-4319-b54d-073b822a7830</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Translation Request/00_Translation-Request_GoToTR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a660b9b-1688-4c0d-99df-79567e7b780d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Translation Request/01_Translation-Request_Validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be55f589-e0ba-4340-8645-2dbc2e1ed0cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Translation Request/02_Translation-Request_Languages</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>186bce21-5964-4fc0-a2e5-3c9c994611c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Translation Request/03-0_Translation-Request_Summary</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8febda35-8a8c-4a31-a607-b5ee44624643</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Translation Request/03-1_Translation-Request_Add-Manager</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee26cf17-3c0b-445d-82db-ec0495338255</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Translation Request/04_Translation-Request_Additional-Assets</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ecd48d0-ddd4-4225-9615-072c80345e5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Translation Request/05_Translation-Request_Final-Summary</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a4e0ea6-7add-46e3-86a2-6aff3766882d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PM,TM,Translator/01_PM_AssignTranslationManagers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fae7dac-5a9d-489a-a12a-2fea21532e01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PM,TM,Translator/03_TM_AssignTranslators</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc085618-f82f-4e9b-9598-3fd37a928c6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PM,TM,Translator/05_Translator_Complete-TR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7bed4ae-d71d-4de4-a4c6-c3c3f5975cbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PM,TM,Translator/06_TM_Unlock-TR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b302b7b-2efc-49aa-9055-33256b4aa6ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PM,TM,Translator/07_TM_Reject-TR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3289f7a-b4cc-4f0c-a947-91780e4f2713</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PM,TM,Translator/08_Translator_Complete-Rejected-TR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8329850-cb5d-4380-9f21-a243eef43a58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PM,TM,Translator/09_Translator_Complete-Unlocked-TR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec1737ac-e44c-4f93-9c8b-821d2f00c66b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PM,TM,Translator/10_TM_Approve</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf2b9d39-fabd-4146-8bd7-0186e050bb30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PM,TM,Translator/11_PM_Approve</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
