<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Edit Project and Mapping Assistance</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-13T09:35:35</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f79846c9-c3fb-4c4f-92f6-1306c1d50c37</testSuiteGuid>
   <testCaseLink>
      <guid>776ef0a7-8582-4742-9a18-6b1c9a20a9cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_CNP_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b0a608e-c32f-4070-a73d-a1f1396705d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/00_CNP_Upload_XML_File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>921810bc-7818-44e2-ac12-d63fe6901ff7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/01_CNP_Language_naming</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5231a305-fcc2-46d8-b30a-29954614bb1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/02_CNP_Selecting_languages</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c6b55db-edb6-44df-9282-557c6e53147e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/03_CNP_Properties</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5a912ac-fec3-40b9-b292-83325f6685d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/04_CNP_Meta-Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37070bde-4b33-4c94-a9f4-bb2acb202187</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/05_CNP_Selecting_alternative_fonts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0c683f9-40fc-44a2-9cb5-e63ebda74ff0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/06_CNP_Inviting_users_to_project</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af5dc910-b89a-4e38-9379-e7ec700f0214</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/07_GoTo_Edit-Project</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>55d1803b-89b5-4a4a-8a74-2b0465174ff8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/08_Edit-Project_Project-Information</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b393148d-9237-4e29-af76-06bbd035b2cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/09_Edit-Project_Languages-Naming</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>119e4928-99a8-4ad6-8512-61a6256cea55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/10_Edit-Project_Select-Languages</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e51bcae-d0fe-4e10-bbae-d383414df58b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/11_Edit-Project_Properties</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efcfad8d-4ac5-4729-a1d4-d49f724bb7c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/12_Edit-Project_Meta-Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e74d879-e2c4-4ecf-a0b4-661a07f765ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/13_Edit-Project_Recalculation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d6b907f-3356-4f8f-a029-0a8b2bbf2973</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/14_Edit-Project_Alternative-Fonts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea97e4a4-0fe0-4029-a3e5-38fb83153f28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/15_Edit-Project_Invite-Users</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4e906f7-498c-4be4-9706-d6143cbb418a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Edit Project/16_Edit-Project_CheckIfProjectExist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81536d68-496a-4034-8921-c6f88d420712</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Mapping Assistance/00_MoveToMappingAssistance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50d20070-6d13-4039-abb1-a9fbf32c1b70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Mapping Assistance/01_Mapping-Assistance_Validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>679bd1c0-4dc9-4ac0-a7ba-d1841eda5132</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Mapping Assistance/02_Mapping-Assistance_Select-Language</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d7e6131d-7c45-4a0d-b372-6fa834f00e79</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Mapping Assistance/03_Mapping-Assistance_Other-Settings</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a632eab4-0652-4d20-9a41-29deaf1df179</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Mapping Assistance/04_Mapping-Assistance_Confirm-On-Dashboard</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
