<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>STC - Edge</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-06-27T13:54:06</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c9d8d368-742e-44cf-b736-3368788c2a39</testSuiteGuid>
   <testCaseLink>
      <guid>51203b33-04c1-4621-b61c-c1a4b2b08ef1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_STC_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db3b231b-ca0a-4ae6-875d-611065d1ab54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/00-1_STC_GoToSTC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7a167af-fe4e-43c4-a331-b35489644fb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/Edge/00_STC_Adding_main_group - Edge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f7c3e69-7705-4e41-b5b1-ce868daf99ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/Edge/01_STC_Adding_group_to_selected_group - Edge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b570a0fc-d839-4131-8bac-05522d16922d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/Edge/02_STC_Adding_text_to_selected_group - Edge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3307aa4-d277-4803-80a3-ddcc1d6701be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/Edge/03_STC_Adding_short_form - Edge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>484a79a8-6f59-4abf-b715-fd95ed0a1138</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/Edge/04_STC_Deleting_short_form_not_used_in_project - Edge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9393f9d4-6b35-43b0-a5da-81a55954eb47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/05_STC_Deleting_STC_entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe5d416b-9c76-4286-8d51-f51736cadb44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/06_STC_Editing_group</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1495c26-2858-4c36-ab19-e8571d659026</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/07_STC_Deleting_group</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>808680c9-fed7-4f67-8520-6d03965db96c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/08_STC_settings</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78208132-7fbd-40cd-a1c7-b3da3dc3914c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/09_STC_Searching</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
