<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Mapping</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-24T11:44:02</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>395588d2-973d-4e1c-9c7d-6f0e7854f970</testSuiteGuid>
   <testCaseLink>
      <guid>54598f8d-86a5-452f-a25c-64182edf7a41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_CNP_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8aa0c5e1-2433-4628-b321-00514a15f66b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LabelFilterManager/OpenProject</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d9a3c54-4253-42c9-aae3-174af6c9cbea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mapping/Mapping_search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f058a701-a738-4592-a384-56960f3e99d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mapping/Mapping_assign</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54269621-80c4-480c-a939-a38eeae7b4b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mapping/Mapping_open_STC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8575c65-91b8-4314-983c-ffc1a05c6224</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mapping/Mapping_add_shortform_STC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4502bad0-8b76-465e-9648-ebee9c121ac6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mapping/Mapping_delete_shortform_STC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>487877a8-d311-4dae-801b-26e366d81f9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mapping/Mapping_add_shortform_MT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>438d675c-bbde-4222-9f21-ad3047ce5d33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mapping/Mapping_delete_shortform_MT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4e6609a-09dd-410b-bba2-d8804194b4f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mapping/Mapping_remove</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
