<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>STC-group-subgroup-text</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-24T12:39:09</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b049a634-91f8-4b59-bbdf-53240ef88012</testSuiteGuid>
   <testCaseLink>
      <guid>51203b33-04c1-4621-b61c-c1a4b2b08ef1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_STC_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db3b231b-ca0a-4ae6-875d-611065d1ab54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/00-1_STC_GoToSTC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ad428d0-aca9-4c7b-a3b9-b8f86e973849</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/00_STC_Adding_main_group</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>961b6f99-af3b-4d3e-b352-e2fcc32c39af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/01_STC_Adding_group_to_selected_group</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eeeafe5f-ddc6-46da-b03c-7e6cba4f28bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/02_STC_Adding_text_to_selected_group</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efdefed9-3b39-482f-bfab-ea4ba5a6658d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/03_STC_Adding_short_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>076812f0-5f03-4f8f-9c6c-e112f31eb590</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/04_STC_Deleting_short_form_not_used_in_project</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9393f9d4-6b35-43b0-a5da-81a55954eb47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/05_STC_Deleting_STC_entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe5d416b-9c76-4286-8d51-f51736cadb44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/06_STC_Editing_group</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1495c26-2858-4c36-ab19-e8571d659026</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/07_STC_Deleting_group</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
