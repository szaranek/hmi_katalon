<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MT, Spellcheck, LC</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-21T10:15:17</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e3475f78-b29d-4261-a36b-e2c0c833e66d</testSuiteGuid>
   <testCaseLink>
      <guid>ef707184-237e-4f17-8a60-fb81e90b3602</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_CNP_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bdf798f3-f64b-4a06-9c62-21b4145b35cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/00_CNP_Upload_XML_File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e36c23e-3fa8-43a3-b488-4f962488cefd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/01_CNP_Language_naming</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c5ada8a-9033-4e8c-a5d7-34d4a6ff53f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/02_CNP_Selecting_languages</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9489fbc-4849-48aa-a056-c6967259667e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/03_CNP_Properties</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8fd22c7-fbd7-41ba-975b-09f39e238bee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/04_CNP_Meta-Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2032f80c-85e9-4c92-a716-30f73555cb02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/05_CNP_Selecting_alternative_fonts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c13fa279-d21a-496d-b1ee-d5024bc19a87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/06_CNP_Inviting_users_to_project</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8040dd6-2a45-4743-9a7c-384f0c6ebb93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/00_MT_GoToMT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc73d737-96b5-415c-96d2-6d171e9b837a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Spellcheck/Spellcheck</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed98f3c1-d2e5-4252-b710-4fde6f4b06fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Length Calculator/Length Calculator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>291a6a74-fa8e-4a02-88c1-07c83faceaa1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Adding Translation/MT_Commenting in manual translation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>034ec2a1-ce9a-469f-84e2-a8a185381494</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Adding Translation/MT_Undo Redo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64113f04-a835-4fa9-8aee-f3df2200efbb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Adding Translation/MT_Adding translation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a6a0dcc-c34a-4de2-a020-2bdbf4314ae2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Translation Memory/Translation-Memory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6f84961-ebe7-4ada-bc81-59512c5b4202</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Placeholders/Placeholders</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df88ab9e-c3d8-4781-8080-1c1970c40015</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_STC_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd670b9d-5884-41dd-a7ff-d7965f7e9ab9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Delete Project/Delete-Project</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
