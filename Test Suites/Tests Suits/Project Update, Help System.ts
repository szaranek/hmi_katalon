<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Project Update, Help System</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-26T15:04:12</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>24cc2c7e-f525-43ae-a31c-784ebe2cd5c2</testSuiteGuid>
   <testCaseLink>
      <guid>493b7f92-b3d6-4729-98bb-ffce0c4f8330</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_STC_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>444b3899-558a-4373-85ef-98cc29b4e067</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Project Update/Create New Project - PU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33c79798-b785-4bf6-9f10-4e42095a0cbc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Project Update/00_Project-Update-Go-To-PU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc29f79d-a1a8-4826-ba21-17d5e419a4b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Project Update/01_Project-Update-Upload-XML</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74e0e707-fbc1-47f3-9b8a-35ada85f89bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Project Update/02_Project-Update-Schema-Update</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bee5dfb4-6ead-4ebf-aa63-e23a8975f930</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Project Update/03_Project-Update-Upload-Lc-Fonts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65f62f2f-bebd-438c-9e76-76d83b112eba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Project Update/04_Project-Update-Languages-Naming</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>424c489b-668d-4b03-98c0-b5b36b56aa90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Project Update/05_Project-Update-Select-Languages</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5dc4758-c902-403b-87e4-45ee1e083f05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/Project Update/06_Project-Update-Summary</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>478c3640-c0e5-450b-bbf9-288d820bba93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Help System/00_Help-System_LoginToHelpSystem</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>157d93b8-9261-4df2-b327-fd89850b6b7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Help System/01_Help-System_GoToHelpSystem</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01ff8457-1e06-4e61-b88e-a45acae61674</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Help System/02_Help-System_Navigation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0318a506-3ad0-486b-9e30-98caafb6d624</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Help System/03_Help-System_Add-Root-Group</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8d09cc2-6189-467e-927b-73c0d0463212</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Help System/04_Help-System_Add-Subgroup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe2ad588-954c-4b26-a99a-ddc231d7fac7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Help System/05_Help-System_Add-Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6f0fe70-708b-4cf3-a93b-cf3b32d0fca6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Help System/06_Help-System_Edit-Delete-Page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
