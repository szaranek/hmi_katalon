<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Create Project - FF</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-08-02T10:00:24</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e5668dc4-54f1-435e-8463-33089c1c8b91</testSuiteGuid>
   <testCaseLink>
      <guid>0f8902f0-b72b-44a4-9c52-d7066bf5eded</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_CNP_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96842bd0-a77a-48f0-a1e1-b3f08ff9af77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/00_CNP_Upload_XML_File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a25965eb-f420-43a5-b30e-ca24aa160de6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/02_CNP_Selecting_languages</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>008066e8-71a2-4c37-a264-921825f2d37a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/03_CNP_Properties</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4a5ac0b-65c3-47c7-b4b2-dcf23ad674e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/04_CNP_Meta-Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ea7746a-f154-4fb8-8ffc-a4438d1e087c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/05_CNP_Selecting_alternative_fonts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83206e1d-5f93-4d50-8062-482afe549b5d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CNP/06_CNP_Inviting_users_to_project</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3f2b64c-50de-421e-bf5c-e2a1ce74d63a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_STC_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bca37a30-f331-4d21-b60a-f1bb35cc8519</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Delete Project/Delete-Project</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
