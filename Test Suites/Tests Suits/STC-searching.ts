<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>STC-searching</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b6e73a83-6a87-46d5-971c-cca35fd08548</testSuiteGuid>
   <testCaseLink>
      <guid>51203b33-04c1-4621-b61c-c1a4b2b08ef1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_STC_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db3b231b-ca0a-4ae6-875d-611065d1ab54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/00-1_STC_GoToSTC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>350598cb-74c7-4617-a1ff-7470ebf96ba2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/09_STC_Searching</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
