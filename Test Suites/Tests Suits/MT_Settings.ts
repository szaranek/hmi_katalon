<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MT_Settings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-10-16T15:17:37</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1c842ca6-5287-45a3-b183-cf28a8c64b40</testSuiteGuid>
   <testCaseLink>
      <guid>51ff246c-cec8-40e2-bb36-5cc299c5494f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_CNP_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b32301d6-ee56-415c-8a71-b96582813f7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LabelFilterManager/OpenProject</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3d6a936-30ee-4df7-af02-74b5fa1c1847</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/MT_Settings/Tooltips_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67ebc44a-b37f-4e0a-a95c-f2e32f448c3f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/MT_Settings/Mapping_settings</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8738d766-1628-4951-8df9-125dca50c80b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/MT_Settings/TM_penalties</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af51953f-24da-4efe-9d10-ba8eab404c96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/MT_Settings/Columns_configuration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71e606d9-90ea-46c3-b4c2-60b5fbf7acf5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/MT_Settings/Terminology_settings</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
