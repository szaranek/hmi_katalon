<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>STC-settings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>88197247-2655-4e2a-9a8b-b25a714acd3c</testSuiteGuid>
   <testCaseLink>
      <guid>51203b33-04c1-4621-b61c-c1a4b2b08ef1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_STC_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db3b231b-ca0a-4ae6-875d-611065d1ab54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/00-1_STC_GoToSTC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>808680c9-fed7-4f67-8520-6d03965db96c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STC/08_STC_settings</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
