<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MT-searching</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1d0d2af1-fb19-4e41-b0f7-50729e269129</testSuiteGuid>
   <testCaseLink>
      <guid>d5b70441-7fac-4579-9594-cee506bb4117</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/00_CNP_Log_in_to_HMI_tool</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8fd5b8c-b561-4eb4-a8b3-18ae5eb05990</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/00_MT_GoToMT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>671be5e5-2c65-45fe-a39e-91e4b03fa8a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MT/09_MT_Search</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
